#include "field.h"

#include <cassert>
#include <queue>

namespace hive {

namespace {

std::unordered_set<HexPos> GetBugMoves(Bug bug, const Cells& cells,
                                       const HexPos& from);

// Whether all neighbor cells of the given hexpos are occupied.
bool IsSurrounded(const Cells& cells, const HexPos& hexpos) {
  const auto& neighbors = hexpos.GetNeighbors();
  return std::all_of(
      neighbors.begin(), neighbors.end(),
      [&cells](const HexPos& neighbor) { return cells.Has(neighbor); });
}

// Whether the position is connected to at least one other cell.
bool HasAnyNeighbors(const Cells& cells, const HexPos& hexpos) {
  const auto& neighbors = hexpos.GetNeighbors();
  return std::any_of(
      neighbors.begin(), neighbors.end(),
      [&cells](const HexPos& neighbor) { return cells.Has(neighbor); });
}

// Whether all neighbor cells are of the same player or empty.
// An empty cell like that is a valid position for adding new tiles.
bool AllNeighborsAreEmptyOrOfPlayer(const Cells& cells, const HexPos& hexpos,
                                    Who who) {
  const auto& neighbors = hexpos.GetNeighbors();
  return std::all_of(neighbors.begin(), neighbors.end(),
                     [&cells, who](const HexPos& neighbor) {
                       auto maybe_tile = cells.GetTopTile(neighbor);
                       return !maybe_tile || maybe_tile->GetWho() == who;
                     });
}

// Checks whether a tile can be moved in the given direction.
// A cell can't move in the direction if on both sides of the direction, the
// cells are occupied, preventing the tile from sliding into the target
// position. A cell on one of the sides of the direction must be occupied to
// ensure the hive stays connected.
bool IsMoveConnectedNotBlocked(const Cells& cells, const HexPos& from,
                               int dir) {
  return !cells.Has(from.GetDirNeighbor(HexPos::NextDir(dir))) ^
         !cells.Has(from.GetDirNeighbor(HexPos::PrevDir(dir)));
}

// Whether the cell on any side of the move is occupied.
bool IsMoveConnectedToAnother(const Cells& cells, const HexPos& from, int dir) {
  return cells.Has(from.GetDirNeighbor(HexPos::NextDir(dir))) ||
         cells.Has(from.GetDirNeighbor(HexPos::PrevDir(dir)));
}

// Whether the cell can't be moved on top of the field, because of being blocked
// by stacks of size>=2 on both sides of the direction.
bool IsMoveConnectedBlockedByStacks(const Cells& cells, const HexPos& from,
                                    int dir) {
  const HexPos p1 = from.GetDirNeighbor(HexPos::NextDir(dir));
  const HexPos p2 = from.GetDirNeighbor(HexPos::PrevDir(dir));
  return cells.StackHeight(p1) >= 2 && cells.StackHeight(p2) >= 2;
}

// The queen bee can move one space per turn in any direction.
std::unordered_set<HexPos> GetQueenMoves(const Cells& cells,
                                         const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
    const HexPos neighbor = hexpos.GetDirNeighbor(dir);
    if (!cells.Has(neighbor) && IsMoveConnectedNotBlocked(cells, hexpos, dir)) {
      result.insert(neighbor);
    }
  }
  return result;
}

// Generate possible spider moves by DFSing, prevent spider from backtracking on
// the already visited positions.
void GenSpiderMoves(const Cells& cells, const HexPos& hexpos,
                    std::unordered_set<HexPos>* history,
                    std::unordered_set<HexPos>* possible_moves) {
  const int kSpiderRange = 3;
  if (history->size() == kSpiderRange + 1) {
    possible_moves->insert(hexpos);
    return;
  }
  for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
    const HexPos neighbor = hexpos.GetDirNeighbor(dir);
    if (!history->count(neighbor) && !cells.Has(neighbor) &&
        IsMoveConnectedNotBlocked(cells, hexpos, dir)) {
      history->insert(neighbor);
      GenSpiderMoves(cells, neighbor, history, possible_moves);
      history->erase(neighbor);
    }
  }
}

// The spider moves 3 spaces per turn, but the moves must be in the same
// direction.
std::unordered_set<HexPos> GetSpiderMoves(const Cells& cells,
                                          const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  std::unordered_set<HexPos> history = {hexpos};
  GenSpiderMoves(cells, hexpos, &history, &result);
  return result;
}

// Grasshopper jumps over any number (>=1) of pieces in any direction to the
// next unoccupied space.
std::unordered_set<HexPos> GetGrasshopperMoves(const Cells& cells,
                                               const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
    HexPos neighbor = hexpos.GetDirNeighbor(dir);
    if (!cells.Has(neighbor)) {
      continue;
    }
    // Keep going in the selected direction until the first unoccupied space.
    for (; cells.Has(neighbor); neighbor = neighbor.GetDirNeighbor(dir))
      ;
    result.insert(neighbor);
  }
  return result;
}

// The soldier ant can move any number of spaces in any directions over
// unoccupied spaces.
// Generate possible moves by BFSing from the given position.
std::unordered_set<HexPos> GetAntMoves(const Cells& cells,
                                       const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  std::queue<HexPos> next;
  next.push(hexpos);
  std::unordered_set<HexPos> history{hexpos};
  for (; !next.empty(); next.pop()) {
    const HexPos& c = next.front();
    for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
      const HexPos neighbor = c.GetDirNeighbor(dir);
      if (!history.count(neighbor) && !cells.Has(neighbor) &&
          IsMoveConnectedNotBlocked(cells, c, dir)) {
        result.insert(neighbor);
        next.push(neighbor);
        history.insert(neighbor);
      }
    }
  }
  return result;
}

// The Beetle moves one space per turn, but can move on top of any other cell.
std::unordered_set<HexPos> GetBeetleMoves(const Cells& cells,
                                          const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  const bool elevated = cells.Has(hexpos);
  for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
    const HexPos neighbor = hexpos.GetDirNeighbor(dir);
    bool can = false;
    if (cells.Has(neighbor)) {
      // Beetle can move on top of any other tile.
      can = true;
    } else if (IsMoveConnectedToAnother(cells, hexpos, dir)) {
      can = true;
    } else if (elevated && HasAnyNeighbors(cells, neighbor)) {
      // Going from on top of the field to an unoccupied cell can be done in any
      // direction.
      can = true;
    }
    if (can) {
      result.insert(neighbor);
    }
  }
  return result;
}

// Ladybug moves three spaces: 2 on top of the hive, then one down.
std::unordered_set<HexPos> GetLadybugMoves(const Cells& cells,
                                           const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  for (const HexPos& neighbor1 : hexpos.GetNeighbors()) {
    if (!cells.Has(neighbor1)) {
      // First move must be on top of the hive.
      continue;
    }
    for (const HexPos& neighbor2 : neighbor1.GetNeighbors()) {
      if (!cells.Has(neighbor2)) {
        // Second move must be on top of the hive.
        continue;
      }
      for (const HexPos& neighbor3 : neighbor2.GetNeighbors()) {
        if (cells.Has(neighbor3)) {
          // Third move must be down to an unoccupied cell.
          continue;
        }
        result.insert(neighbor3);
      }
    }
  }
  return result;
}

// Pillbug moves like the queen bee.
std::unordered_set<HexPos> GetPillbugMoves(const Cells& cells,
                                           const HexPos& hexpos) {
  return GetQueenMoves(cells, hexpos);
}

// Mosquito moves like any bug it touches.
// If mosquito only touches another mosquito, then it can't move.
// If mosquito moved like a beetle and is now on top of the hive, it can only
// continue moving as a beetle.
std::unordered_set<HexPos> GetMosquitoMoves(const Cells& cells,
                                            const HexPos& hexpos) {
  std::unordered_set<HexPos> result;
  std::unordered_set<Bug> processed_bugs;
  for (const HexPos& neighbor : hexpos.GetNeighbors()) {
    auto maybe_tile = cells.GetTopTile(neighbor);
    if (!maybe_tile) {
      continue;
    }
    const Bug bug = maybe_tile->GetBug();
    if (bug != Bug::MOSQUITO && !processed_bugs.count(bug)) {
      processed_bugs.insert(bug);
      const std::unordered_set<HexPos> bug_result =
          GetBugMoves(bug, cells, hexpos);
      result.insert(bug_result.begin(), bug_result.end());
    }
  }
  return result;
}

// Dispatches the moves of the given bug.
std::unordered_set<HexPos> GetBugMoves(Bug bug, const Cells& cells,
                                       const HexPos& from) {
  switch (bug) {
    case Bug::QUEEN: {
      return GetQueenMoves(cells, from);
    }
    case Bug::SPIDER: {
      return GetSpiderMoves(cells, from);
    }
    case Bug::GRASSHOPPER: {
      return GetGrasshopperMoves(cells, from);
    }
    case Bug::ANT: {
      return GetAntMoves(cells, from);
    }
    case Bug::BEETLE: {
      return GetBeetleMoves(cells, from);
    }
    case Bug::LADYBUG: {
      return GetLadybugMoves(cells, from);
    }
    case Bug::PILLBUG: {
      return GetPillbugMoves(cells, from);
    }
    case Bug::MOSQUITO: {
      return GetMosquitoMoves(cells, from);
    }
  }
  qFatal("Unknown Bug value");
  return {};
}

// Whether the hive remains connected after removing a tile from the given
// position.
bool CanRemoveCell(const Cells& cells, const HexPos& hexpos) {
  Cells cells_copy = cells;
  cells_copy.RemoveTopTile(hexpos);
  return cells_copy.AreConnected();
}

}  // namespace

Field::Field(Field&& field) : cells_(std::move(field.cells_)) {}

Field& Field::operator=(Field&& other) {
  cells_ = std::move(other.cells_);
  return *this;
}

Field Field::Clone() const {
  Field cloned;
  cloned.cells_ = cells_.Clone();
  return cloned;
}

bool Field::IsQueenPlaced(Who who) const {
  for (const auto& cell : cells_) {
    for (const Tile& tile : cell.second) {
      if (tile == Tile(who, Bug::QUEEN)) {
        return true;
      }
    }
  }
  return false;
}

bool Field::IsQueenAlive(Who who) const {
  for (const auto& kv : cells_) {
    const HexPos& hexpos = kv.first;
    for (const Tile& tile : kv.second) {
      if (tile.GetBug() == Bug::QUEEN && tile.GetWho() == who) {
        return !IsSurrounded(cells_, hexpos);
      }
    }
  }
  return true;
}

bool Field::AreQueensAlive() const {
  bool white_alive = IsQueenAlive(Who::WHITE);
  bool black_alive = IsQueenAlive(Who::BLACK);
  return white_alive && black_alive;
}

bool Field::HasPillbugAbility(const HexPos& hexpos) const {
  const Tile& tile = cells_.GetTopTileOrDie(hexpos);
  if (tile.GetBug() == Bug::PILLBUG) {
    return true;
  }
  // The only other bug that can have a pillbug ability is a mosquito next to a
  // pillbug.
  if (tile.GetBug() != Bug::MOSQUITO) {
    return false;
  }
  for (const HexPos& neighbor : hexpos.GetNeighbors()) {
    const auto maybe_tile = cells_.GetTopTile(neighbor);
    if (maybe_tile && maybe_tile->GetBug() == Bug::PILLBUG) {
      return true;
    }
  }
  return false;
}

std::unordered_set<HexPos> Field::GetPossibleMoves(const HexPos& from) const {
  const Tile& tile = cells_.GetTopTileOrDie(from);
  if (!IsQueenPlaced(tile.GetWho())) {
    // Can't move any tiles if the queen bee isn't placed yet.
    return {};
  }

  const Bug bug = tile.GetBug();
  Cells cells = cells_;
  cells.RemoveTopTile(from);
  if (!cells.AreConnected()) {
    // Can't move a tile if removing it would break the hive into several
    // unconnected parts.
    return {};
  }

  return GetBugMoves(bug, cells, from);
}

std::unordered_set<HexPos> Field::GetPossiblePillbugAbilityMoves(
    const HexPos& pillbug_hexpos,
    const std::experimental::optional<HexPos>& last_moved_tile) const {
  std::unordered_set<HexPos> result;
  if (HasPillbugAbility(pillbug_hexpos)) {
    for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
      const HexPos neighbor = pillbug_hexpos.GetDirNeighbor(dir);
      if ((cells_.StackHeight(neighbor) == 1 &&
           (!last_moved_tile || neighbor != last_moved_tile) &&
           !IsMoveConnectedBlockedByStacks(cells_, pillbug_hexpos, dir) &&
           CanRemoveCell(cells_, neighbor)) ||
          (!cells_.Has(neighbor) &&
           !IsMoveConnectedBlockedByStacks(cells_, pillbug_hexpos, dir))) {
        result.insert(neighbor);
      }
    }
  }

  // Check that 'result' contains at least one target to be moved and at least
  // one place to move it to. Otherwise, consider that pillbug ability cannot be
  // aplied at this moment.
  bool has_tiles = false;
  bool has_empty = false;
  for (const HexPos& hexpos : result) {
    if (cells_.Has(hexpos)) {
      has_tiles = true;
    } else {
      has_empty = true;
    }
  }

  if (!has_tiles || !has_empty) {
    result.clear();
  }
  return result;
}

std::unordered_set<HexPos> Field::GetPossibleAddLocations(
    Bug bug, Who who, int turn_number) const {
  if (cells_.IsEmpty()) {
    // Let's make it simpler by restricting the initial move to (0,0).
    return {HexPos(0, 0)};
  }
  if (cells_.Size() == 1) {
    // First turn of player BLACK violates the placing rule. The new tile can be
    // added to any location next to the white's tile.
    auto hexposs = cells_.begin()->first.GetNeighbors();
    return std::unordered_set<HexPos>(hexposs.begin(), hexposs.end());
  }
  // The queen must be placed on the turn #4, if it wasn't placed yet.
  if (turn_number == kTurnWhenMustPlaceQueen && bug != Bug::QUEEN &&
      !IsQueenPlaced(who)) {
    return {};
  }
  std::unordered_set<HexPos> result;
  for (const auto& kv : cells_) {
    Q_ASSERT(!kv.second.empty());
    const Tile& top_tile = kv.second.back();
    if (top_tile.GetWho() == who) {
      for (const HexPos& neighbor : kv.first.GetNeighbors()) {
        if (!cells_.Has(neighbor) &&
            AllNeighborsAreEmptyOrOfPlayer(cells_, neighbor, who)) {
          result.insert(neighbor);
        }
      }
    }
  }
  return result;
}

void Field::AddTile(Bug bug, Who who, const HexPos& to) {
  const Tile tile(who, bug);
  cells_.AddTile(to, tile);
}

void Field::MoveTile(const HexPos& from, const HexPos& to) {
  auto maybe_tile = cells_.GetTopTile(from);
  Q_ASSERT(maybe_tile);
  cells_.AddTile(to, *maybe_tile);
  cells_.RemoveTopTile(from);
}

bool Field::AreAnyMovesAvailable(
    Who who, const std::experimental::optional<HexPos>& last_moved_tile,
    bool can_add_tiles) const {
  // If any tile can be added, then a QUEEN tile can also be added, therefore
  // check only if a QUEEN tile can be added.
  if (can_add_tiles && !GetPossibleAddLocations(Bug::QUEEN, who, 1).empty()) {
    return true;
  }
  for (const auto& kv : cells_) {
    const Tile& top_tile = kv.second.back();
    if (top_tile.GetWho() != who) {
      continue;
    }
    if (!GetPossibleMoves(kv.first).empty()) {
      return true;
    }
    if (HasPillbugAbility(kv.first) &&
        !GetPossiblePillbugAbilityMoves(kv.first, last_moved_tile).empty()) {
      return true;
    }
  }
  return false;
}

QDebug operator<<(QDebug dbg, const Field& field) {
  QDebugStateSaver saver(dbg);
  return dbg << "Field{cells: " << field.GetCells() << '}';
}

}  // namespace hive
