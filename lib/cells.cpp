#include "cells.h"

#include <queue>
#include <unordered_set>

namespace hive {

Cells::Cells() {}

Cells::Cells(const Cells& another) : cells_(another.cells_) {}

Cells::Cells(Cells&& another) : cells_(std::move(another.cells_)) {}

Cells& Cells::operator=(const Cells& another) {
  cells_ = another.cells_;
  return *this;
}

Cells& Cells::operator=(Cells&& another) {
  cells_ = std::move(another.cells_);
  return *this;
}

Cells Cells::Clone() const {
  Cells cloned;
  cloned.cells_ = cells_;
  return cloned;
}

void Cells::Clear() { cells_.clear(); }

bool Cells::IsEmpty() const { return cells_.empty(); }

int Cells::Size() const { return static_cast<int>(cells_.size()); }

std::experimental::optional<Tile> Cells::GetTopTile(
    const HexPos& hexpos) const {
  auto it = cells_.find(hexpos);
  if (it == cells_.end()) {
    return std::experimental::nullopt;
  }
  Q_ASSERT(!it->second.empty());
  return it->second.back();
}

const Tile& Cells::GetTopTileOrDie(const HexPos& hexpos) const {
  auto it = cells_.find(hexpos);
  Q_ASSERT(it != cells_.end());
  Q_ASSERT(!it->second.empty());
  return it->second.back();
}

bool Cells::Has(const HexPos& hexpos) const { return StackHeight(hexpos) > 0; }

int Cells::StackHeight(const HexPos& hexpos) const {
  auto it = cells_.find(hexpos);
  if (it == cells_.end()) {
    return 0;
  }
  return static_cast<int>(it->second.size());
}

bool Cells::AreConnected() const {
  if (Size() < 2) {
    // Trivial sets of cells are connected.
    return true;
  }
  const HexPos& start = cells_.begin()->first;
  std::unordered_set<HexPos> visited{start};
  std::queue<HexPos> next;
  next.push(start);
  for (; !next.empty(); next.pop()) {
    const HexPos& hexpos = next.front();
    for (int dir = 0; dir < HexPos::kNumDirs; dir++) {
      const HexPos neighbor = hexpos.GetDirNeighbor(dir);
      if (Has(neighbor) && !visited.count(neighbor)) {
        visited.insert(neighbor);
        next.push(neighbor);
      }
    }
  }
  return visited.size() == cells_.size();
}

void Cells::RemoveTopTile(const HexPos& hexpos) {
  auto it = cells_.find(hexpos);
  Q_ASSERT(it != cells_.end());
  Q_ASSERT(!it->second.empty());
  it->second.pop_back();
  if (it->second.empty()) {
    cells_.erase(it);
  }
}

void Cells::AddTile(const HexPos& hexpos, const Tile& tile) {
  cells_[hexpos].push_back(tile);
}

Cells::const_iterator Cells::begin() const { return cells_.begin(); }

Cells::const_iterator Cells::end() const { return cells_.end(); }

QDebug operator<<(QDebug os, const Cells& cells) {
  os << '{';
  for (const auto& kv : cells) {
    os << kv << ';';
  }
  return os << '}';
}

}  // namespace hive
