#include "move.h"

#include <sstream>

namespace hive {

/*static*/ Move Move::AddTile(Bug bug, const HexPos& to) {
  Move move;
  move.move_type_ = MoveType::ADD_TILE;
  move.to_ = to;
  move.bug_ = bug;
  return move;
}

/*static*/ Move Move::MoveTile(const HexPos& from, const HexPos& to) {
  Move move;
  move.move_type_ = MoveType::MOVE;
  move.from_ = from;
  move.to_ = to;
  return move;
}

/*static*/ Move Move::PillbugAbility(const HexPos& pillbug_pos,
                                     const HexPos& from, const HexPos& to) {
  Move move;
  move.move_type_ = MoveType::PILLBUG_ABILITY;
  move.pillbug_pos_ = pillbug_pos;
  move.from_ = from;
  move.to_ = to;
  return move;
}

/*static*/ Move Move::Skip() {
  Move move;
  move.move_type_ = MoveType::SKIP;
  return move;
}

Bug Move::GetBug() const {
  Q_ASSERT(bug_);
  return *bug_;
}

Move::MoveType Move::GetMoveType() const { return move_type_; }

const HexPos& Move::GetFromPos() const {
  Q_ASSERT(from_);
  return *from_;
}

const HexPos& Move::GetToPos() const {
  Q_ASSERT(to_);
  return *to_;
}

const HexPos& Move::GetPillbugAbilityPos() const {
  Q_ASSERT(pillbug_pos_);
  return *pillbug_pos_;
}

const std::experimental::optional<Bug>& Move::GetOptionalBug() const {
  return bug_;
}

const std::experimental::optional<HexPos>& Move::GetOptionalFromPos() const {
  return from_;
}

const std::experimental::optional<HexPos>& Move::GetOptionalToPos() const {
  return to_;
}

const std::experimental::optional<HexPos>& Move::GetOptionalPillbugAbilityPos()
    const {
  return pillbug_pos_;
}

bool Move::operator==(const Move& another) const {
  return bug_ == another.bug_ && from_ == another.from_ && to_ == another.to_ &&
         pillbug_pos_ == another.pillbug_pos_;
}

template <class T>
std::ostream& operator<<(std::ostream& os,
                         const std::experimental::optional<T>& t) {
  if (!t) {
    os << "nullopt";
  } else {
    os << *t;
  }
  return os;
}

std::ostream& operator<<(std::ostream& os, const Move& move) {
  return os << "Move{bug: " << move.GetOptionalBug()
            << ", from: " << move.GetOptionalFromPos()
            << ", to: " << move.GetOptionalToPos()
            << ", pillbug_ability: " << move.GetOptionalPillbugAbilityPos()
            << "}";
}

QDebug operator<<(QDebug dbg, const Move& move) {
  std::stringstream ss;
  ss << move;
  QDebugStateSaver saver(dbg);
  dbg.nospace() << QString::fromStdString(ss.str());
  return dbg;
}

}  // namespace hive
