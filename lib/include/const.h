// Enums representing various aspects of the game: bugs, players, game mode.

#ifndef CONST_H
#define CONST_H

#include <QtDebug>

namespace hive {

// The kind of bug on a tile.
enum class Bug {
  QUEEN,
  SPIDER,
  BEETLE,
  GRASSHOPPER,
  ANT,
  MOSQUITO,
  LADYBUG,
  PILLBUG,
};

// Player ID, white or black.
enum class Who {
  WHITE,
  BLACK,
};

// Returns BLACK given WHITE and WHITE given BLACK.
Who GetEnemy(Who who);

// Game mode determines the number of bug pieces of each kind given to players
// at the beginning of the game.
enum class GameMode {
  NORMAL,
  EXPANSION,  // NORMAL + Mosquito + Ladybug.
  PILLBUG,    // EXPANSION + Pillbug.
};

QDebug operator<<(QDebug dbg, Bug bug);
QDebug operator<<(QDebug dbg, Who who);
QDebug operator<<(QDebug dbg, GameMode game_mode);

std::ostream& operator<<(std::ostream& os, Bug bug);
std::ostream& operator<<(std::ostream& os, Who who);
std::ostream& operator<<(std::ostream& os, GameMode game_mode);

}  // namespace hive

namespace std {
template <>
struct hash<hive::Bug> {
  size_t operator()(const hive::Bug& bug) const {
    return static_cast<size_t>(bug);
  }
};

template <>
struct hash<hive::Who> {
  size_t operator()(const hive::Who& who) const {
    return static_cast<size_t>(who);
  }
};
}  // namespace std

#endif  // CONST_H
