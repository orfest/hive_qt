// Manages the QPainterPath for drawing cells as hexagons.
// Hexagons are drawn "standing" on the bottom edge:
//   __    _
//  /  \__/ ...
//  \__/  \_

#ifndef CELLSHAPE_H
#define CELLSHAPE_H

#include <vector>

#include <QBrush>
#include <QPainterPath>
#include <QPointF>

#include "const.h"

namespace hive {

class CellShape {
 public:
  // Half the height of the cell.
  static constexpr const qreal HALF_DY = 30;

  // Distance between centers of adjacent cells along the X-axis.
  static constexpr const qreal DX = 60;

  // Width of a cell along the X-axis.
  static constexpr const qreal X_SIZE = DX * 4 / 3.0;

  // Getter for a singleton instance.
  static const CellShape& GetInstance();

  // Returns the QPainterPath for the hex cell.
  const QPainterPath& GetCellPath() const;

 private:
  CellShape();

  QPainterPath cell_path_;
};

}  // namespace hive

#endif  // CELLSHAPE_H
