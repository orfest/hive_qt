// Controller notifies players about the progress of the game.
// Controller encapsulates the variety of possible player types (network, human
// UI, AI), from the Game.

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <unordered_map>

#include "const.h"
#include "game.h"
#include "player.h"

namespace hive {

class Controller {
 public:
  // Doesn't take ownership.
  Controller(Game* game);
  virtual ~Controller() {}

  // Doesn't take ownership.
  void SetPlayers(Player* white_player, Player* black_player);

  // Notify the first player that the game starts.
  virtual void Start();

  // Make the requsted move.
  // The move must be valid.
  virtual void MakeMove(const Move& move);

 private:
  Controller(const Controller&) = delete;
  Controller(Controller&&) = delete;
  Controller& operator=(const Controller&) = delete;
  Controller& operator=(Controller&&) = delete;

  // Notify all players that the game ended.
  void NotifyGameOver();

  // Notify players that the next move is expected from 'who'.
  void NotifyNextMove(Who who);

  Game* const game_;
  std::unordered_map<Who, Player*> players_;
};

}  // namespace hive

#endif  // CONTROLLER_H
