// Represents the state of the field, provides methods to mutating its state.
// Assumes that hive is valid.
// Provides bug-specific functionality.

#ifndef FIELD_H
#define FIELD_H

#include <experimental/optional>
#include <unordered_set>

#include <QtGlobal>

#include "cells.h"
#include "const.h"
#include "hexpos.h"
#include "math.h"

namespace hive {

class Field {
 public:
  Field() {}
  Field(Field&&);

  Field& operator=(Field&&);

  Field Clone() const;

  // Const-accessor for the cells.
  const Cells& GetCells() const { return cells_; }

  // Returns whether the queen tile of the player 'who' is already on the field.
  bool IsQueenPlaced(Who who) const;

  // Returns whether none of the queens are surrounded.
  bool AreQueensAlive() const;

  bool IsQueenAlive(Who who) const;

  // Returns whether the bug at the top of the stack can act as a pillbug.
  // A bug can act as a pillbug if the bug is a pillbug, or a mosquito next to a
  // pillbug.
  bool HasPillbugAbility(const HexPos& hexpos) const;

  // Returns a set of position where a tile from the top of the given position
  // can be validly moved.
  std::unordered_set<HexPos> GetPossibleMoves(const HexPos& from) const;

  // Returns a set of positions that can be affected by a bug with a pillbug
  // ability at the given position. This takes into account the rule that the
  // last moved tile cannot be moved by a pillbug. The set of possible positions
  // contains both empty and non-empty positions, this should be interpreted as
  // any tile from a non-empty position can be moved to any empty position.
  std::unordered_set<HexPos> GetPossiblePillbugAbilityMoves(
      const HexPos& pillbug_hexpos,
      const std::experimental::optional<HexPos>& last_moved_tile) const;

  // Returns where a bug of the given player can be placed.
  // This takes into account the rule that the queen bee must be placed within
  // first several turns.
  std::unordered_set<HexPos> GetPossibleAddLocations(Bug bug, Who who,
                                                     int turn_number) const;

  // Returns whether any tile can be added to the game, or any tile can be
  // moved, or any pillbug ability can be used. 'can_add_tiles' represents
  // whether the player has any tiles left that can be placed.
  bool AreAnyMovesAvailable(
      Who who, const std::experimental::optional<HexPos>& last_moved_tile,
      bool can_add_tiles) const;

  // Moves a tile from the top of the stack at the first given position on top
  // of the (possibly empty) stack at the second given position.
  void MoveTile(const HexPos& from, const HexPos& to);

  // Adds a tile on top of the (possibly empty) stack at the given coordinate.
  void AddTile(Bug bug, Who who, const HexPos& to);

 private:
  // Rules of the game require the queen to placed within the first 4 turns.
  static const int kTurnWhenMustPlaceQueen = 3;

  Field(const Field&) = delete;
  Field& operator=(const Field&) = delete;

  // The tiles representing the field.
  Cells cells_;
};
QDebug operator<<(QDebug dbg, const Field& field);

}  // namespace hive

#endif  // FIELD_H
