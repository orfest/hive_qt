// HexPos represents a coordinate on the hexagonal grid.
//   ^              _
//   |   (1,1)      /|
// (1,0)       (0,2)
//   |   (0,1)
// (0,0)       (-1,2)

#ifndef HEXPOS_H
#define HEXPOS_H

#include <functional>
#include <iostream>
#include <vector>

#include <QtDebug>

namespace hive {

class HexPos {
 public:
  // Returns (dir+1) or (dir-1) modulo kNumDirs.
  static int NextDir(int dir);
  static int PrevDir(int dir);

  // Number of neighbors of each cell.
  static const int kNumDirs = 6;

  // Constructs a valid coordinate;
  HexPos(int x, int y);

  int GetX() const;
  int GetY() const;

  // Methods to get cell neighbors, either for a certain direction or all of
  // them.
  HexPos GetDirNeighbor(int dir) const;
  std::vector<HexPos> GetNeighbors() const;

  // Only valid coordinates can be compared.
  // Invalid coordinates trigger an assertion.
  bool operator<(const HexPos& hexpos) const;
  bool operator==(const HexPos& hexpos) const;
  bool operator!=(const HexPos& hexpos) const;

 private:
  // Constructs an invalid coordinate.
  HexPos();

  int x_;
  int y_;
};
QDebug operator<<(QDebug dbg, const HexPos& hexpos);
std::ostream& operator<<(std::ostream& os, const HexPos& hexpos);

}  // namespace hive

namespace std {
template <>
struct hash<hive::HexPos> {
  size_t operator()(const hive::HexPos& hexpos) const {
    return (static_cast<size_t>(hexpos.GetX()) * 11311 +
            static_cast<size_t>(hexpos.GetY()));
  }
};
}  // namespace std

#endif  // HEXPOS_H
