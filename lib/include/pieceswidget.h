// PiecesWidget shows a list of tiles available to a player.

#ifndef PIECESWIDGET_H
#define PIECESWIDGET_H

#include <experimental/optional>

#include <QBrush>
#include <QPen>
#include <QWidget>

#include "const.h"
#include "game.h"
#include "piecesset.h"
#include "selectionstate.h"

namespace hive {

class PiecesWidget : public QWidget {
  Q_OBJECT
 public:
  explicit PiecesWidget(QWidget* parent = nullptr);
  // Doesn't take ownership of any of objects passed by pointer.
  PiecesWidget& SetGame(Game* game);
  PiecesWidget& SetPiecesSet(const PiecesSet* pieces_set);
  PiecesWidget& SetSelectionState(SelectionState* selection_state);
  PiecesWidget& SetWho(Who who);

 protected:
  void paintEvent(QPaintEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;

 private:
  // The vertical overlap of tiles when a player has more than one bug of a
  // certain type.
  static constexpr const qreal kVerticalOverlap = 0.7;

  // Compute how the tiles should be scaled to make all tiles fit into the
  // widget.
  qreal GetScale(int n) const;

  // Returns at which bug the player clicked.
  std::experimental::optional<Bug> GetClickedBug(const QPoint& pos) const;

  Who GetWho() const;

  const PiecesSet* pieces_set_ = nullptr;  // Not owned.
  Game* game_ = nullptr;                   // Not owned.
  std::experimental::optional<Who> who_;
  // Manages the selection state.
  SelectionState* selection_state_ = nullptr;  // Not owned.

  qreal scale_ = 1;
};

}  // namespace hive

#endif  // PIECESWIDGET_H
