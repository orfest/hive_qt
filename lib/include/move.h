// Representation of a move done by a player.

#ifndef MOVE_H
#define MOVE_H

#include <experimental/optional>
#include <ostream>

#include <QtDebug>

#include "const.h"
#include "hexpos.h"

namespace hive {

class Move {
 public:
  enum class MoveType {
    ADD_TILE,
    MOVE,
    PILLBUG_ABILITY,
    SKIP,
  };
  static Move AddTile(Bug bug, const HexPos& to);
  static Move MoveTile(const HexPos& from, const HexPos& to);
  static Move PillbugAbility(const HexPos& pillbug_pos, const HexPos& from,
                             const HexPos& to);
  static Move Skip();

  Move(const Move&) = default;
  Move(Move&&) = default;
  Move& operator=(const Move&) = default;
  Move& operator=(Move&&) = default;
  ~Move() {}

  MoveType GetMoveType() const;
  Bug GetBug() const;
  const HexPos& GetFromPos() const;
  const HexPos& GetToPos() const;
  const HexPos& GetPillbugAbilityPos() const;

  const std::experimental::optional<Bug>& GetOptionalBug() const;
  const std::experimental::optional<HexPos>& GetOptionalFromPos() const;
  const std::experimental::optional<HexPos>& GetOptionalToPos() const;
  const std::experimental::optional<HexPos>& GetOptionalPillbugAbilityPos()
      const;

  bool operator==(const Move& another) const;

 private:
  Move() = default;

  MoveType move_type_;
  std::experimental::optional<Bug> bug_;
  std::experimental::optional<HexPos> to_;
  std::experimental::optional<HexPos> from_;
  std::experimental::optional<HexPos> pillbug_pos_;
};
QDebug operator<<(QDebug dbg, const Move& move);
std::ostream& operator<<(std::ostream& os, const Move& move);

}  // namespace hive

#endif  // MOVE_H
