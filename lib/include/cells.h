// Container for a set of tile stacks. Cells doesn't know anything about the
// tiles it stores.

#ifndef CELLS_H
#define CELLS_H

#include <experimental/optional>
#include <list>
#include <unordered_map>

#include "const.h"
#include "hexpos.h"
#include "tile.h"

namespace hive {

class Cells {
 public:
  // Internal container representing the tile stacks.
  using Map = std::unordered_map<HexPos, std::list<Tile>>;
  // Expose an iterator type of the internal container.
  using const_iterator = Map::const_iterator;

  // Methods to support iterating over the cells.
  const_iterator begin() const;
  const_iterator end() const;

  Cells();
  Cells(const Cells& another);
  Cells(Cells&& another);

  Cells& operator=(const Cells& cells);
  Cells& operator=(Cells&& cells);

  Cells Clone() const;

  // Returns true iff the field is empty.
  bool IsEmpty() const;

  // Returns the number of non-empty cells.
  int Size() const;

  // Clears the set of cells.
  void Clear();

  // Returns the top tile of a stack or nullopt.
  std::experimental::optional<Tile> GetTopTile(const HexPos& hexpos) const;

  // Asserts that a stack is non-empty, return the top tile of the stack.
  const Tile& GetTopTileOrDie(const HexPos& hexpos) const;

  // Returns whether the stack at the given coordinate is a non-empty.
  bool Has(const HexPos& hexpos) const;

  // Returns the height of the stack at the given coordinate.
  int StackHeight(const HexPos& hexpos) const;

  // Returns whether the cells form a single connected component.
  bool AreConnected() const;

  // Asserts that the stack is not empty and removes the top tile from the
  // stack. Removes a cell from the map if the stack becomes empty.
  void RemoveTopTile(const HexPos& hexpos);

  // Adds a tile to the top of the stack.
  void AddTile(const HexPos& hexpos, const Tile& tile);

 private:
  // Mapping between coordinates and tile stacks.
  // Top stack tiles are at the end of the vector.
  Map cells_;
};
QDebug operator<<(QDebug os, const Cells& cells);

}  // namespace hive

#endif  // CELLS_H
