// FieldWidget renders the tiles, the grid, and it processes mouse actions.

#ifndef FIELDWIDGET_H
#define FIELDWIDGET_H

#include <experimental/optional>
#include <unordered_set>
#include <vector>

#include <QBrush>
#include <QPaintEvent>
#include <QPen>
#include <QStaticText>
#include <QWidget>

#include "controller.h"
#include "coordinateconverter.h"
#include "field.h"
#include "game.h"
#include "selectionstate.h"

namespace hive {

class FieldWidget : public QWidget {
  Q_OBJECT
 public:
  explicit FieldWidget(QWidget* parent = nullptr);
  // Doesn't take ownership of any of the objects passed by pointer.
  FieldWidget& SetCells(const Cells* cells);
  FieldWidget& SetController(Controller* controller);
  FieldWidget& SetGame(Game* game);
  FieldWidget& SetSelectionState(SelectionState* selection_state);

 protected:
  void paintEvent(QPaintEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;

 private:
  using ViewPoint = QPoint;
  using ModelPoint = CoordinateConverter::ModelPoint;

  // Returns a position on the hexagonal grid that was clicked.
  HexPos GetClickedHexPos(const QPoint& pos) const;

  // Conversion between ViewPoint and ModelPoint.
  ViewPoint ModelPointToViewPoint(const ModelPoint& point) const;
  ModelPoint ViewPointToModelPoint(const ViewPoint& point) const;

  // Whether the given cell of a hexagonal grid has any part within the
  // rectangular region.
  bool IsVisibleCell(const HexPos& hexpos, const QRect& region) const;

  // Fills the widget background, renders grid..
  void Render(QPainter* painter);

  // Renders grid - empty cells and tiles.
  void RenderGrid(const std::unordered_set<HexPos>& possible_moves,
                  QPainter* painter) const;

  // Renders a single cell, which can be empty, contain a tile, be selected, be
  // a possible move location.
  void RenderCell(const HexPos& hexpos,
                  const std::unordered_set<HexPos>& possible_moves,
                  QPainter* painter) const;

  void RenderDraw(QPainter* painter) const;
  void RenderWin(Who who, QPainter* painter) const;
  void RenderStaticText(const QStaticText& text, int x,
                        QPainter* painter) const;

  // If possible marks the tile as selected.
  void MaybeSelectBug(const HexPos& hexpos);

  // If possible adds a tile to the field.
  void MaybeAddTile(const HexPos& hexpos);

  // If possible moves a tile from one position to another according to the
  // current selection.
  void MaybeMoveTile(const HexPos& hexpos,
                     const std::unordered_set<HexPos>& possible_moves);

  // If possible selects a bug with a pillbug ability.
  void MaybeSelectBugWithPillbugAbility(const HexPos& hexpos);

  // If possible applies the pillbug ability according to the current selection.
  void MaybeApplyPillbugAbility(
      const HexPos& hexpos, const std::unordered_set<HexPos>& possible_moves);

  Controller* controller_ = nullptr;
  Game* game_ = nullptr;          // Not owned.
  const Cells* cells_ = nullptr;  // Not owned.
  // Current selection state.
  SelectionState* selection_state_ = nullptr;  // Not owned.

  // Viewport parameters: effects of panning and zooming.
  QPoint center_ = QPoint(0, 0);
  qreal zoom_ = 1;

  QStaticText draw_text_;
  QStaticText win_white_text_;
  QStaticText win_black_text_;
};

}  // namespace hive

#endif  // FIELDWIDGET_H
