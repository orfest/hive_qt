// SelectionState manages the state of selected bugs in different widgets.
// This provides a separation of layers between the model and the view layers.

#ifndef SELECTIONSTATE_H
#define SELECTIONSTATE_H

#include <experimental/optional>
#include <unordered_map>

#include <QWidget>

#include "const.h"
#include "hexpos.h"

namespace hive {

enum class SelectionType {
  NONE,
  ADD_TILE,
  MOVE,
  PILLBUG_ABILITY,
};

class SelectionState {
 public:
  // Doesn't take ownership.
  SelectionState(QWidget* main_window);

  SelectionType GetSelectionType() const;
  const std::experimental::optional<Bug> GetAddBugSelection() const {
    return add_bug_selection_;
  }
  const std::experimental::optional<HexPos>& GetMoveBugSelection() const {
    return move_bug_selection_;
  }
  const std::experimental::optional<HexPos>& GetPillbugSelection() const {
    return pillbug_selection_;
  }

  void ClearSelection();
  void SetAddBugSelection(Bug bug);
  void SetMoveBugSelection(const HexPos& hexpos);
  void SetPillbugSelection(const HexPos& hexpos);
  void SetPillbugAndTargetSelection(const HexPos& pillbug_pos,
                                    const HexPos& target_pos);

 private:
  void ClearSelectionInternal();
  void Repaint();

  std::experimental::optional<Bug> add_bug_selection_;
  std::experimental::optional<HexPos> move_bug_selection_;
  std::experimental::optional<HexPos> pillbug_selection_;
  QWidget* const main_window_;  // Not owned.
};

}  // namespace hive

#endif  // SELECTIONSTATE_H
