#ifndef MATH_H
#define MATH_H

#include <QtGlobal>

namespace hive {

// mod and div operations with strict mathematical definitions: r>=0.
// For example, fgood_mod(-1,3) == 2.
// For example, fgood_div(-1,3) == -1.
qreal ExactRealMod(qreal a, qreal b);
qreal ExactRealDiv(qreal a, qreal b);

// Returns true iff x is even.
bool IsIntEven(int x);

}  // namespace hive

#endif  // MATH_H
