// CoordinateConverter is a utility class to convert between HexPos and
// ModelPoint, where ModelPoint is a 2d real-valued position on the field after
// all zooming and panning is applied.

#ifndef COORDINATECONVERTER_H
#define COORDINATECONVERTER_H

#include <QPoint>

#include "hexpos.h"

namespace hive {

class CoordinateConverter {
 public:
  using ModelPoint = QPointF;

  // Conversion between hexagonal grid positions and ModelPoint.
  static ModelPoint HexPosToModelPoint(const HexPos& hexpos);
  static HexPos ModelPointToHexPos(const ModelPoint& point);

 private:
  CoordinateConverter() {}
};

}  // namespace hive

#endif  // COORDINATECONVERTER_H
