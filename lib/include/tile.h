// Tile is a hexagon piece with a bug on it.

#ifndef TILE_H
#define TILE_H

#include <QtDebug>

#include "const.h"

namespace hive {

class Tile {
 public:
  Tile(Who who, Bug bug);

  Who GetWho() const { return who_; }
  Bug GetBug() const { return bug_; }

  // Returns true iff both who_ and bug_ are valid, i.e. not UNKNOWN.
  bool IsValid() const;

  bool operator==(const Tile& tile) const;
  bool operator!=(const Tile& tile) const;

 private:
  Who who_;
  Bug bug_;
};
QDebug operator<<(QDebug os, const Tile& tile);

}  // namespace hive

#endif  // TILE_H
