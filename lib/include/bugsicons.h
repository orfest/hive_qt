// BugsIcons loads and stores icons for each of the bug.
// BugsIcons has a singleton instance to guarantee that icons are loaded exactly
// once.

#ifndef BUGSICONS_H
#define BUGSICONS_H

#include <unordered_map>

#include <QBrush>
#include <QImage>

#include "const.h"

namespace hive {

class BugsIcons {
 public:
  static const BugsIcons& GetInstance();

  QImage GetIcon(Bug bug, Who who) const;
  QBrush GetCellBrush(Who who) const;
  const QBrush& GetPossibleMoveBrush() const { return possible_move_brush_; }
  const QBrush& GetPillbugMoveBrush() const { return pillbug_move_brush_; }

 private:
  BugsIcons();
  std::unordered_map<Bug, QImage> icons_;
  const QBrush possible_move_brush_ = QBrush(QColor(0, 255, 0, 100));
  const QBrush pillbug_move_brush_ = QBrush(QColor(0, 0, 255, 100));
};

}  // namespace hive

#endif  // BUGSICONS_H
