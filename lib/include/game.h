// Game contains the state of the game: field, #turn, active player, etc.

#ifndef GAME_H
#define GAME_H

#include <memory>
#include <unordered_map>
#include <unordered_set>

#include "const.h"
#include "field.h"
#include "game.h"
#include "move.h"
#include "piecesset.h"

namespace hive {

class Game {
 public:
  Game();
  Game(Game&& other);
  virtual ~Game() {}

  Game Clone() const;

  // Instantiates PiecesSets, resets turn counter.
  void Init(GameMode game_mode);

  // Whether the game has ended.
  virtual bool IsGameOver() const;

  std::experimental::optional<Who> GetWinner() const;

  // Makes the given move. The move must be valid.
  virtual void MakeMove(const Move& move);

  // Returns the player whose turn it currently is.
  virtual Who GetWhoseTurn() const;

  // Returns the Player object of the given player.
  const PiecesSet& GetPiecesSet(Who who) const;
  PiecesSet& GetMutablePiecesSet(Who who);

  const Field& GetField() const;

  // Whether the queen bee of the current player is already placed.
  bool IsQueenPlaced() const;

  // Whether the top tile of at the given position has a pillbug ability.
  bool HasPillbugAbility(const HexPos& from) const;

  // Returns possible moves of the current player.
  std::unordered_set<HexPos> GetPossibleMoves(const HexPos& from) const;

  // Returns possible pillbug moves of the current player.
  std::unordered_set<HexPos> GetPossiblePillbugAbilityMoves(
      const HexPos& pillbug_hexpos) const;

  // Returns possible locations where the current player can add a tile.
  std::unordered_set<HexPos> GetPossibleAddLocations(Bug bug) const;

 private:
  Game(const Game&) = delete;
  Game& operator=(const Game&) = delete;
  Game& operator=(Game&&) = delete;

  // Checks if the game ended and updates game_over_ accordingly.
  void UpdateGameOver();

  // Returns the Player object of the player whose turn it is.
  PiecesSet& GetCurrentPlayer();
  const PiecesSet& GetCurrentPlayer() const;

  // Give turn to the next player.
  void NextHalfTurn();

  // Whether the current player has any possible moves.
  bool AreAnyMovesAvailable() const;

  // Add a tile at the given position.
  void AddTile(Bug bug, const HexPos& hexpos);

  // Move a tile of the current player from one position to another.
  void MoveTile(const HexPos& from, const HexPos& to);

  // Ends player's turn without moving any tiles.
  void SkipMove();

  Field field_;
  GameMode game_mode_;
  Who active_player_ = Who::WHITE;
  // Turn number, only turns of WHITE increase the turn number.
  int turn_number_ = 0;
  std::unordered_map<Who, PiecesSet> pieces_sets_;
  // The tile that was moved last.
  std::experimental::optional<HexPos> last_moved_hexpos_;
  // Whether any more moves are possible.
  bool game_over_ = false;
};

}  // namespace hive

#endif  // GAME_H
