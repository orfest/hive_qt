// PiecesSet represents the state of available tiles of a player.

#ifndef PIECES_SET_H
#define PIECES_SET_H

#include <map>

#include "const.h"

namespace hive {

class PiecesSet {
 public:
  explicit PiecesSet(GameMode game_mode);

  bool HasAnyBugs() const;
  const std::map<Bug, int>& GetBugs() const { return has_; }

  // Remove a bug from the player's set of available bugs.
  void RemoveBug(Bug bug);

 private:
  std::map<Bug, int> has_;
};

QDebug operator<<(QDebug dbg, const PiecesSet& player);

}  // namespace hive

#endif  // PIECES_SET_H
