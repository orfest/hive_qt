// Represents a player that uses GUI to select the next move.

#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include "player.h"
#include "selectionstate.h"

namespace hive {

class HumanPlayer : public Player {
 public:
  // Doesn't take ownership.
  explicit HumanPlayer(SelectionState* selection_state);

 private:
    // Clear the selection state.
    void GetAndMakeMove() override;
    void EndMoveImpl() override;

  SelectionState* const selection_state_;  // Not owned.
};

}  // namespace hive

#endif  // HUMANPLAYER_H
