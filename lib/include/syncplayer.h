// An abstraction for a class of players that determine the next move
// synchronously.

#ifndef SYNCPLAYER_H
#define SYNCPLAYER_H

#include <experimental/optional>

#include "controller.h"
#include "move.h"
#include "player.h"

namespace hive {

class SyncPlayer : public Player {
 public:
  // Doesn't take ownership.
  explicit SyncPlayer(Controller* controller);

  // Selects a move to make.
    Move GetNextMove() { return GetNextMoveImpl();}

 protected:
    // MakeMove gets the next move using GetNextMove() and makes it.
    void GetAndMakeMove() override;

    // Does nothing.
    void EndMoveImpl() override;

    virtual Move GetNextMoveImpl() = 0;

  Controller* const controller_;  // Not owned.
};

}  // namespace hive

#endif  // SYNCPLAYER_H
