// Main window of the application, contains all other widgets and actions.
// Manages game objects: Game, Controller, etc.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>

#include "controller.h"
#include "game.h"
#include "player.h"
#include "selectionstate.h"

namespace Ui {
class MainWindow;
}  // namespace Ui

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

 private:
  Ui::MainWindow *ui;

  std::unique_ptr<hive::Game> game_;
  std::unique_ptr<hive::SelectionState> selection_state_;
  std::unique_ptr<hive::Player> white_player_;
  std::unique_ptr<hive::Player> black_player_;
  std::unique_ptr<hive::Controller> controller_;
};

#endif  // MAINWINDOW_H
