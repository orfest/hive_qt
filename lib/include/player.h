#ifndef PLAYER_H
#define PLAYER_H

namespace hive {

class Player {
 public:
  Player() {}
  virtual ~Player() {}

  void MakeMove() { return GetAndMakeMove(); }
  void EndMove() { return EndMoveImpl(); }

private:
  virtual void GetAndMakeMove() = 0;
  virtual void EndMoveImpl() = 0;
};

}  // namespace hive

#endif  // PLAYER_H
