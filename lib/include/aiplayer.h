// A player that selects one of the moves using heuristics.
// TODO(nkurtov): AI is limited to making random valid moves.

#ifndef AIPLAYER_H
#define AIPLAYER_H

#include "const.h"
#include "controller.h"
#include "game.h"
#include "piecesset.h"
#include "syncplayer.h"

namespace hive {

class AiPlayer : public SyncPlayer {
 public:
  AiPlayer(Game* game,             // Not owned.
           Controller* controller  // Not owned
  );

 private:
  Move GetNextMoveImpl() override;

  Game* const game_;
};

}  // namespace hive

#endif  // AIPLAYER_H
