#include "coordinateconverter.h"

#include "cellshape.h"
#include "math.h"

namespace hive {

//   --+------+------+
//  /  |3 \ 4 |   /  |
// /   |___\__|__/   |
// \   | 1 /  |  \   |
//  \  |  / 2 |   \  |
//   --+------+------+
// Draw horizontal and vertical lines through centers of hexagons. The lines
// form rectangles. Determine to which rectangle the given point belongs, and
// then determine to which hexagon that corresponds.
/*static*/ HexPos CoordinateConverter::ModelPointToHexPos(
    const ModelPoint& point) {
  const int kx = static_cast<int>(ExactRealDiv(point.x(), CellShape::DX));
  const int ky = static_cast<int>(ExactRealDiv(point.y(), CellShape::HALF_DY));
  const bool even = IsIntEven(kx ^ ky);
  const qreal rx = ExactRealMod(point.x(), CellShape::DX);
  const qreal ry = ExactRealMod(point.y(), CellShape::HALF_DY);
  int qx = kx;
  int qy = ky;
  if (even) {
    // Cases 1 and 2.
    if (rx > (CellShape::DX * 2) / 3.0) {
      qx++;
      qy++;
      // Case 2.
    } else if (rx > CellShape::DX / 3.0 &&
               ry > CellShape::HALF_DY -
                        (3 * rx - CellShape::DX) * CellShape::HALF_DY /
                            static_cast<qreal>(CellShape::DX)) {
      qx++;
      qy++;
      // Case 2.
    }
  } else {
    // Cases 3 and 4.
    qy++;
    if (rx > (CellShape::DX * 2) / 3.0) {
      qx++;
      qy--;
      // Case 4.
    } else if (rx > CellShape::DX / 3.0 &&
               ry < (3 * rx - CellShape::DX) * CellShape::HALF_DY /
                        static_cast<qreal>(CellShape::DX)) {
      qx++;
      qy--;
      // Case 4.
    }
  }
  Q_ASSERT(IsIntEven(qx ^ qy));
  const int res_y = qx;
  const int res_x = (qy - qx) / 2;
  return HexPos(res_x, res_y);
}

/*static*/ CoordinateConverter::ModelPoint
CoordinateConverter::HexPosToModelPoint(const HexPos& hexpos) {
  return ModelPoint(CellShape::DX * hexpos.GetY(),
                    2 * CellShape::HALF_DY * hexpos.GetX() +
                        CellShape::HALF_DY * hexpos.GetY());
}

}  // namespace hive
