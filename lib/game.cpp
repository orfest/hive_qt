#include "game.h"

namespace hive {

Game::Game() {}

Game::Game(Game&& other)
    : field_(std::move(other.field_)),
      game_mode_(other.game_mode_),
      active_player_(other.active_player_),
      turn_number_(other.turn_number_),
      pieces_sets_(other.pieces_sets_),
      last_moved_hexpos_(other.last_moved_hexpos_),
      game_over_(other.game_over_) {}

Game Game::Clone() const {
  Game cloned;
  cloned.field_ = field_.Clone();
  cloned.game_mode_ = game_mode_;
  cloned.active_player_ = active_player_;
  cloned.turn_number_ = turn_number_;
  cloned.pieces_sets_ = pieces_sets_;
  cloned.last_moved_hexpos_ = last_moved_hexpos_;
  cloned.game_over_ = game_over_;
  return cloned;
}

void Game::Init(GameMode game_mode) {
  game_mode_ = game_mode;
  turn_number_ = 0;
  active_player_ = Who::WHITE;
  game_over_ = false;
  last_moved_hexpos_ = std::experimental::nullopt;
  pieces_sets_.clear();

  for (Who who : {Who::WHITE, Who::BLACK}) {
    pieces_sets_.emplace(who, PiecesSet(game_mode_));
  }
}

PiecesSet& Game::GetMutablePiecesSet(Who who) {
  auto it = pieces_sets_.find(who);
  Q_ASSERT(it != pieces_sets_.end());
  return it->second;
}

const PiecesSet& Game::GetPiecesSet(Who who) const {
  return const_cast<Game*>(this)->GetMutablePiecesSet(who);
}

const Field& Game::GetField() const { return field_; }

bool Game::IsGameOver() const { return game_over_; }

Who Game::GetWhoseTurn() const { return active_player_; }

std::experimental::optional<Who> Game::GetWinner() const {
  Q_ASSERT(IsGameOver());
  const bool white_alive = field_.IsQueenAlive(Who::WHITE);
  const bool black_alive = field_.IsQueenAlive(Who::BLACK);
  if (black_alive && !white_alive) {
    return Who::BLACK;
  }
  if (white_alive && !black_alive) {
    return Who::WHITE;
  }
  if (!white_alive && !black_alive) {
    return std::experimental::nullopt;
  }
  Q_ASSERT(false);
  return std::experimental::nullopt;
}

bool Game::IsQueenPlaced() const {
  return field_.IsQueenPlaced(GetWhoseTurn());
}

void Game::UpdateGameOver() {
  if (!field_.AreQueensAlive()) {
    game_over_ = true;
  }
}

std::unordered_set<HexPos> Game::GetPossibleMoves(const HexPos& from) const {
  if (game_over_) {
    return {};
  }
  return field_.GetPossibleMoves(from);
}

std::unordered_set<HexPos> Game::GetPossiblePillbugAbilityMoves(
    const HexPos& pillbug_hexpos) const {
  if (game_over_) {
    return {};
  }
  return field_.GetPossiblePillbugAbilityMoves(pillbug_hexpos,
                                               last_moved_hexpos_);
}

std::unordered_set<HexPos> Game::GetPossibleAddLocations(Bug bug) const {
  if (game_over_) {
    return {};
  }
  return field_.GetPossibleAddLocations(bug, GetWhoseTurn(), turn_number_);
}

void Game::AddTile(Bug bug, const HexPos& hexpos) {
  GetCurrentPlayer().RemoveBug(bug);
  field_.AddTile(bug, GetWhoseTurn(), hexpos);
  last_moved_hexpos_ = hexpos;
  NextHalfTurn();
}

void Game::MoveTile(const HexPos& from, const HexPos& to) {
  field_.MoveTile(from, to);
  last_moved_hexpos_ = to;
  NextHalfTurn();
}

PiecesSet& Game::GetCurrentPlayer() {
  auto it = pieces_sets_.find(GetWhoseTurn());
  Q_ASSERT(it != pieces_sets_.end());
  return it->second;
}

const PiecesSet& Game::GetCurrentPlayer() const {
  return const_cast<Game*>(this)->GetCurrentPlayer();
}

bool Game::HasPillbugAbility(const HexPos& from) const {
  return field_.HasPillbugAbility(from);
}

void Game::NextHalfTurn() {
  if (active_player_ == Who::BLACK) {
    turn_number_++;
  }
  active_player_ = GetEnemy(active_player_);
  UpdateGameOver();
}

bool Game::AreAnyMovesAvailable() const {
  return field_.AreAnyMovesAvailable(GetWhoseTurn(), last_moved_hexpos_,
                                     GetCurrentPlayer().HasAnyBugs());
}

void Game::MakeMove(const Move& move) {
  switch (move.GetMoveType()) {
    case Move::MoveType::SKIP:
      SkipMove();
      return;
    case Move::MoveType::ADD_TILE:
      AddTile(move.GetBug(), move.GetToPos());
      return;
    case Move::MoveType::MOVE:
      MoveTile(move.GetFromPos(), move.GetToPos());
      return;
    case Move::MoveType::PILLBUG_ABILITY:
      MoveTile(move.GetFromPos(), move.GetToPos());
      return;
  }
}

void Game::SkipMove() {
  last_moved_hexpos_ = std::experimental::nullopt;
  NextHalfTurn();
}

}  // namespace hive
