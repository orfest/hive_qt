#include "fieldwidget.h"

#include <queue>
#include <unordered_set>

#include <QPainter>
#include <QtDebug>

#include "bugsicons.h"
#include "cellshape.h"

namespace hive {
namespace {

std::unordered_set<HexPos> GetPossibleMoves(
    const Game* game, const SelectionState* selection_state) {
  SelectionType selection_type = selection_state->GetSelectionType();
  switch (selection_type) {
    case SelectionType::NONE:
      return {};
    case SelectionType::MOVE: {
      const auto& maybe_move = selection_state->GetMoveBugSelection();
      Q_ASSERT(maybe_move);
      return game->GetPossibleMoves(*maybe_move);
    }
    case SelectionType::PILLBUG_ABILITY: {
      const auto& maybe_pillbug = selection_state->GetPillbugSelection();
      Q_ASSERT(maybe_pillbug);
      return game->GetPossiblePillbugAbilityMoves(*maybe_pillbug);
    }
    case SelectionType::ADD_TILE: {
      const auto& maybe_bug = selection_state->GetAddBugSelection();
      Q_ASSERT(maybe_bug);
      return game->GetPossibleAddLocations(*maybe_bug);
    }
  }
}

}  // namespace

FieldWidget::FieldWidget(QWidget* parent)
    : QWidget(parent),
      draw_text_("DRAW"),
      win_white_text_("WHITE WINS"),
      win_black_text_("BLACK WINS") {}

FieldWidget& FieldWidget::SetGame(Game* game) {
  Q_ASSERT(!game_);
  game_ = game;
  Q_ASSERT(game_);
  return *this;
}

FieldWidget& FieldWidget::SetCells(const Cells* cells) {
  Q_ASSERT(!cells_);
  cells_ = cells;
  Q_ASSERT(cells_);
  return *this;
}

FieldWidget& FieldWidget::SetSelectionState(SelectionState* selection_state) {
  Q_ASSERT(!selection_state_);
  selection_state_ = selection_state;
  Q_ASSERT(selection_state_);
  return *this;
}

FieldWidget& FieldWidget::SetController(Controller* controller) {
  Q_ASSERT(!controller_);
  controller_ = controller;
  Q_ASSERT(controller_);
  return *this;
}

void FieldWidget::paintEvent(QPaintEvent*) {
  Q_ASSERT(game_);
  QPainter painter;
  painter.begin(this);
  painter.setRenderHint(QPainter::Antialiasing);
  Render(&painter);
  painter.end();
}

void FieldWidget::RenderStaticText(const QStaticText& text, int x,
                                   QPainter* painter) const {
  painter->resetTransform();
  const int kBlackColor = 255;
  painter->fillRect(x - 10, 10, 120, 40,
                    QBrush(QColor(kBlackColor, kBlackColor, kBlackColor)));

  const int kWinPenColor = 10;
  painter->setPen(QPen(QColor(kWinPenColor, kWinPenColor, kWinPenColor)));
  painter->drawStaticText(x, 20, text);
}

void FieldWidget::RenderDraw(QPainter* painter) const {
  RenderStaticText(draw_text_, static_cast<int>(width() * 0.5 - 60), painter);
}

void FieldWidget::RenderWin(Who winner, QPainter* painter) const {
  if (winner == Who::WHITE) {
    RenderStaticText(win_white_text_, 20, painter);
  } else if (winner == Who::BLACK) {
    RenderStaticText(win_black_text_, static_cast<int>(width() - 140), painter);
  } else {
    Q_ASSERT(false);
  }
}

void FieldWidget::Render(QPainter* painter) {
  Q_ASSERT(cells_);
  const int kBackgroundColor = 210;
  painter->fillRect(rect(), QBrush(QColor(kBackgroundColor, kBackgroundColor,
                                          kBackgroundColor)));
  if (cells_->Size() <= 2) {
    // Can't pan an empty field, restrict the viewport to the center of the
    // plane.
    center_ = QPoint(0, 0);
  }
  const std::unordered_set<HexPos> possible_moves =
      GetPossibleMoves(game_, selection_state_);
  RenderGrid(possible_moves, painter);

  if (game_->IsGameOver()) {
    const auto winner = game_->GetWinner();
    if (winner == std::experimental::nullopt) {
      RenderDraw(painter);
    } else {
      RenderWin(*winner, painter);
    }
  }
}

void FieldWidget::RenderCell(const HexPos& hexpos,
                             const std::unordered_set<HexPos>& possible_moves,
                             QPainter* painter) const {
  Q_ASSERT(selection_state_);
  painter->resetTransform();
  const int kGridPenColor = 127;
  painter->setPen(QPen(QColor(kGridPenColor, kGridPenColor, kGridPenColor)));
  const ViewPoint view_point =
      ModelPointToViewPoint(CoordinateConverter::HexPosToModelPoint(hexpos));
  painter->translate(view_point);
  painter->scale(zoom_, zoom_);
  painter->drawPath(CellShape::GetInstance().GetCellPath());
  const std::experimental::optional<Tile> maybe_tile =
      cells_->GetTopTile(hexpos);
  if (maybe_tile) {
    const Tile& tile = *maybe_tile;
    QImage icon =
        BugsIcons::GetInstance().GetIcon(tile.GetBug(), tile.GetWho());
    if (hexpos == selection_state_->GetMoveBugSelection() ||
        hexpos == selection_state_->GetPillbugSelection()) {
      icon.invertPixels();
    }
    painter->fillPath(CellShape::GetInstance().GetCellPath(),
                      BugsIcons::GetInstance().GetCellBrush(tile.GetWho()));
    painter->resetTransform();
    painter->translate(view_point);
    painter->scale(zoom_, zoom_);
    painter->drawImage(static_cast<int>(-icon.width() * 0.5),
                       static_cast<int>(-icon.height() * 0.5), icon);
  }
  if ((selection_state_->GetSelectionType() == SelectionType::MOVE ||
       selection_state_->GetSelectionType() == SelectionType::ADD_TILE) &&
      possible_moves.count(hexpos)) {
    painter->resetTransform();
    painter->translate(view_point);
    painter->scale(zoom_, zoom_);
    painter->fillPath(CellShape::GetInstance().GetCellPath(),
                      BugsIcons::GetInstance().GetPossibleMoveBrush());
  }
  if (selection_state_->GetSelectionType() == SelectionType::PILLBUG_ABILITY &&
      possible_moves.count(hexpos)) {
    painter->resetTransform();
    painter->translate(view_point);
    painter->scale(zoom_, zoom_);
    painter->fillPath(CellShape::GetInstance().GetCellPath(),
                      BugsIcons::GetInstance().GetPillbugMoveBrush());
  }
}

void FieldWidget::RenderGrid(const std::unordered_set<HexPos>& possible_moves,
                             QPainter* painter) const {
  const ModelPoint rect_center =
      ViewPointToModelPoint(rect().center()) + center_;
  const HexPos cell_in_center =
      CoordinateConverter::ModelPointToHexPos(rect_center);
  Q_ASSERT(IsVisibleCell(cell_in_center, rect()));

  // Start rendering cells from a cell in the viewport center, continue moving
  // to its neighbors as long as they are visible.
  std::unordered_set<HexPos> visited = {cell_in_center};
  std::queue<HexPos> next;
  next.push(cell_in_center);
  if (IsVisibleCell(cell_in_center, rect())) {
    RenderCell(cell_in_center, possible_moves, painter);
  }
  for (; !next.empty(); next.pop()) {
    const HexPos cell = next.front();
    for (const HexPos& neighbor : cell.GetNeighbors()) {
      if (visited.count(neighbor)) {
        continue;
      }
      if (IsVisibleCell(neighbor, rect())) {
        RenderCell(neighbor, possible_moves, painter);
        next.push(neighbor);
        visited.insert(neighbor);
      }
    }
  }
}

bool FieldWidget::IsVisibleCell(const HexPos& hexpos,
                                const QRect& region) const {
  const ModelPoint lowleft =
      ViewPointToModelPoint(ViewPoint(region.left(), region.bottom()));
  const ModelPoint topright =
      ViewPointToModelPoint(ViewPoint(region.right(), region.top()));

  const ModelPoint pos = CoordinateConverter::HexPosToModelPoint(hexpos);
  if (pos.y() + CellShape::HALF_DY < topright.y()) {
    return false;
  }
  if (pos.y() - CellShape::HALF_DY > lowleft.y()) {
    return false;
  }
  if (pos.x() - 2 * CellShape::DX / 3.0 > topright.x()) {
    return false;
  }
  if (pos.x() + 2 * CellShape::DX / 3.0 < lowleft.x()) {
    return false;
  }
  return true;
}

FieldWidget::ViewPoint FieldWidget::ModelPointToViewPoint(
    const ModelPoint& point) const {
  return ViewPoint(
      static_cast<int>(width() * 0.5 + (point.x() - center_.x()) * zoom_),
      static_cast<int>(height() * 0.5 + (point.y() - center_.y()) * zoom_));
}

FieldWidget::ModelPoint FieldWidget::ViewPointToModelPoint(
    const ViewPoint& point) const {
  return ModelPoint(
      static_cast<qreal>(center_.x() + (point.x() - width() * 0.5) / zoom_),
      static_cast<qreal>(center_.y() + (point.y() - height() * 0.5) / zoom_));
}

HexPos FieldWidget::GetClickedHexPos(const QPoint& pos) const {
  return CoordinateConverter::ModelPointToHexPos(ViewPointToModelPoint(pos));
}

void FieldWidget::mousePressEvent(QMouseEvent* event) {
  if (game_->IsGameOver()) {
    return;
  }
  if (event->button() == Qt::RightButton) {
    selection_state_->ClearSelection();
    return;
  }
  if (event->button() != Qt::LeftButton) {
    return;
  }
  Q_ASSERT(selection_state_);
  const HexPos clicked = GetClickedHexPos(event->pos());
  const std::unordered_set<HexPos> possible_moves =
      GetPossibleMoves(game_, selection_state_);
  const SelectionType selection_type = selection_state_->GetSelectionType();
  if (selection_type == SelectionType::NONE) {
    MaybeSelectBug(clicked);
  } else if (selection_type == SelectionType::ADD_TILE) {
    MaybeAddTile(clicked);
  } else if (selection_type == SelectionType::MOVE) {
    MaybeMoveTile(clicked, possible_moves);
  } else if (selection_type == SelectionType::PILLBUG_ABILITY) {
    MaybeApplyPillbugAbility(clicked, possible_moves);
  }
}

void FieldWidget::mouseDoubleClickEvent(QMouseEvent* event) {
  if (game_->IsGameOver() || event->button() != Qt::LeftButton ||
      (selection_state_->GetSelectionType() != SelectionType::NONE &&
       selection_state_->GetSelectionType() != SelectionType::MOVE)) {
    return;
  }
  const HexPos clicked = GetClickedHexPos(event->pos());
  MaybeSelectBugWithPillbugAbility(clicked);
}

void FieldWidget::MaybeSelectBug(const HexPos& hexpos) {
  auto maybe_tile = cells_->GetTopTile(hexpos);
  if (maybe_tile && game_->IsQueenPlaced()) {
    const Tile& tile = *maybe_tile;
    if (tile.GetWho() == game_->GetWhoseTurn()) {
      selection_state_->SetMoveBugSelection(hexpos);
    }
  }
}

void FieldWidget::MaybeAddTile(const HexPos& hexpos) {
  // We need to pass the bug, because possible move locations are different for
  // a queen and other bugs at the turn #4.
  const auto& maybe_bug = selection_state_->GetAddBugSelection();
  Q_ASSERT(maybe_bug);
  if (!game_->GetPossibleAddLocations(*maybe_bug).count(hexpos)) {
    return;
  }
  controller_->MakeMove(Move::AddTile(*maybe_bug, hexpos));
  selection_state_->ClearSelection();
}

void FieldWidget::MaybeMoveTile(
    const HexPos& hexpos, const std::unordered_set<HexPos>& possible_moves) {
  if (!possible_moves.count(hexpos)) {
    return;
  }
  controller_->MakeMove(
      Move::MoveTile(*selection_state_->GetMoveBugSelection(), hexpos));
  selection_state_->ClearSelection();
}

void FieldWidget::MaybeApplyPillbugAbility(
    const HexPos& hexpos, const std::unordered_set<HexPos>& possible_moves) {
  auto maybe_tile = cells_->GetTopTile(hexpos);
  if (!selection_state_->GetMoveBugSelection() &&
      possible_moves.count(hexpos) && maybe_tile) {
    selection_state_->SetPillbugAndTargetSelection(
        *selection_state_->GetPillbugSelection(), hexpos);
  } else if (selection_state_->GetMoveBugSelection() &&
             possible_moves.count(hexpos) && !maybe_tile) {
    controller_->MakeMove(
        Move::PillbugAbility(*selection_state_->GetPillbugSelection(),
                             *selection_state_->GetMoveBugSelection(), hexpos));
    selection_state_->ClearSelection();
  }
}

void FieldWidget::MaybeSelectBugWithPillbugAbility(const HexPos& hexpos) {
  auto maybe_tile = cells_->GetTopTile(hexpos);
  if (maybe_tile && game_->IsQueenPlaced()) {
    const Tile& tile = *maybe_tile;
    if (tile.GetWho() == game_->GetWhoseTurn() &&
        game_->HasPillbugAbility(hexpos)) {
      selection_state_->SetPillbugSelection(hexpos);
    }
  }
}

}  // namespace hive
