#include "piecesset.h"

#include <cassert>
#include <utility>

#include <QtGlobal>

#include "math.h"

namespace hive {

PiecesSet::PiecesSet(GameMode game_mode) {
  has_.clear();
  switch (game_mode) {
    case GameMode::PILLBUG:
      has_[Bug::PILLBUG] = 1;
      // FALLTHROUGH_INTENDED
      [[clang::fallthrough]];
    case GameMode::EXPANSION:
      has_[Bug::MOSQUITO] = 1;
      has_[Bug::LADYBUG] = 1;
      // FALLTHROUGH_INTENDED
      [[clang::fallthrough]];
    case GameMode::NORMAL:
      has_[Bug::QUEEN] = 1;
      has_[Bug::SPIDER] = 2;
      has_[Bug::BEETLE] = 2;
      has_[Bug::GRASSHOPPER] = 3;
      has_[Bug::ANT] = 3;
      break;
  }
}

bool PiecesSet::HasAnyBugs() const { return !has_.empty(); }

void PiecesSet::RemoveBug(Bug bug) {
  Q_ASSERT(has_.count(bug) && has_[bug] > 0);
  auto it = has_.find(bug);
  if (it->second == 1) {
    has_.erase(it);
  } else {
    it->second--;
  }
}

QDebug operator<<(QDebug dbg, const PiecesSet& pieces_set) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << "PiecesSet{" << pieces_set.GetBugs() << '}';
  return dbg;
}

}  // namespace hive
