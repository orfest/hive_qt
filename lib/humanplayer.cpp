#include "humanplayer.h"

namespace hive {

HumanPlayer::HumanPlayer(SelectionState* selection_state)
    : selection_state_(selection_state) {}

void HumanPlayer::GetAndMakeMove() { selection_state_->ClearSelection(); }

void HumanPlayer::EndMoveImpl() { selection_state_->ClearSelection(); }

}  // namespace hive
