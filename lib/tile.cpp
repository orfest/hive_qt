#include "tile.h"

namespace hive {

Tile::Tile(Who who, Bug bug) : who_(who), bug_(bug) {}

bool Tile::operator==(const Tile& tile) const {
  return who_ == tile.who_ && bug_ == tile.bug_;
}

bool Tile::operator!=(const Tile& tile) const {
  return who_ != tile.who_ || bug_ != tile.bug_;
}

QDebug operator<<(QDebug dbg, const Tile& tile) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << "Tile{" << tile.GetBug() << ", " << tile.GetWho() << "}";
  return dbg;
}

}  // namespace hive
