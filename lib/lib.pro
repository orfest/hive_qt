QT += widgets
QT -= gui

TARGET = lib
TEMPLATE = lib
CONFIG += c++14 staticlib
QMAKE_CXXFLAGS += -Wall -Wextra

SOURCES += \
    aiplayer.cpp \
    base.cpp \
    bugsicons.cpp \
    cells.cpp \
    cellshape.cpp \
    const.cpp \
    field.cpp \
    fieldwidget.cpp \
    game.cpp \
    hexpos.cpp \
    humanplayer.cpp \
    mainwindow.cpp \
    move.cpp \
    piecesset.cpp \
    pieceswidget.cpp \
    selectionstate.cpp \
    tile.cpp \
    syncplayer.cpp \
    controller.cpp \
    coordinateconverter.cpp

HEADERS += \
    include/aiplayer.h \
    include/bugsicons.h \
    include/cells.h \
    include/cellshape.h \
    include/const.h \
    include/controller.h \
    include/field.h \
    include/fieldwidget.h \
    include/game.h \
    include/hexpos.h \
    include/humanplayer.h \
    include/mainwindow.h \
    include/move.h \
    include/piecesset.h \
    include/pieceswidget.h \
    include/player.h \
    include/selectionstate.h \
    include/syncplayer.h \
    include/tile.h \
    include/coordinateconverter.h \
    include/math.h

FORMS    += mainwindow.ui

DISTFILES += \
    images/ant.png \
    images/beetle.png \
    images/grasshopper.png \
    images/ladybug.png \
    images/mosquito.png \
    images/pillbug.png \
    images/queen.png \
    images/spider.png

RESOURCES += \
    bugs.qrc

INCLUDEPATH += $$PWD/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}
