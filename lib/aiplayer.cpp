#include "aiplayer.h"

#include <random>

#include "move.h"
#include "syncplayer.h"

namespace hive {
namespace {

std::vector<Move> GenPossibleMoves(const Game& game) {
  std::vector<Move> moves;
  const Cells& cells = game.GetField().GetCells();
  for (const auto& kv : game.GetPiecesSet(game.GetWhoseTurn()).GetBugs()) {
    const Bug bug = kv.first;
    const auto add_locations = game.GetPossibleAddLocations(bug);
    for (const HexPos& pos : add_locations) {
      moves.emplace_back(Move::AddTile(bug, pos));
    }
  }

  for (const auto& kv : cells) {
    const HexPos& from = kv.first;
    const Tile& tile = kv.second.back();
    if (tile.GetWho() == game.GetWhoseTurn()) {
      const auto move_locations = game.GetPossibleMoves(from);
      for (const HexPos& to : move_locations) {
        moves.emplace_back(Move::MoveTile(from, to));
      }
    }

    if (game.HasPillbugAbility(from)) {
      const auto move_locations = game.GetPossiblePillbugAbilityMoves(from);
      for (const HexPos& ability_from : move_locations) {
        if (cells.Has(ability_from)) {
          for (const HexPos& ability_to : move_locations) {
            if (!cells.Has(ability_to)) {
              moves.emplace_back(
                  Move::PillbugAbility(from, ability_from, ability_to));
            }
          }
        }
      }
    }
  }
  if (moves.empty()) {
    moves.push_back(Move::Skip());
  }
  return moves;
}

qreal EstimateScore(const Game& game, Who who) {
  bool queen_placed = game.GetField().IsQueenPlaced(who);
  bool queen_paralyzed = false;
  int queen_num_neighbors = 0;
  int queen_num_enemy_neighbors = 0;
  int num_available_tile_types =
      static_cast<int>(game.GetPiecesSet(who).GetBugs().size());
  int num_available_tiles = 0;
  for (const auto& kv : game.GetPiecesSet(who).GetBugs()) {
    num_available_tile_types += kv.second;
  }

  for (const auto& cell : game.GetField().GetCells()) {
    for (const Tile& tile : cell.second) {
      if (tile == Tile(who, Bug::QUEEN)) {
        if (tile != cell.second.back()) {
          queen_paralyzed = true;
        }
        for (const HexPos& neighbor : cell.first.GetNeighbors()) {
          if (game.GetField().GetCells().Has(neighbor)) {
            queen_num_neighbors++;
            if (game.GetField().GetCells().GetTopTileOrDie(neighbor).GetWho() !=
                who) {
              queen_num_enemy_neighbors++;
            }
          }
        }
      }
    }
  }

  qreal result = 0;
  result -= queen_num_enemy_neighbors * 100;
  result -= queen_num_neighbors * 100;
  result -= queen_paralyzed ? 500 : 0;
  result += num_available_tile_types * 40;
  result += num_available_tiles * 100;
  result += queen_placed ? 100 : 0;
  return false;
}

}  // namespace

qreal GetAgnosticGoodness(const Game& game) {
  if (game.IsGameOver()) {
    static const qreal kWin = 1e6;
    auto winner = game.GetWinner();
    if (winner) {
      if (*winner == game.GetWhoseTurn()) {
        return kWin;
      } else {
        return -kWin;
      }
    }
    return 0;
  }
  qreal score_me = EstimateScore(game, game.GetWhoseTurn());
  qreal score_enemy = EstimateScore(game, GetEnemy(game.GetWhoseTurn()));
  return score_me - score_enemy;
}

struct MinimaxResult {
  qreal value;
  Move move;
};

qreal GameGoodness(const Game& game) {
  qreal goodness = GetAgnosticGoodness(game);
  if (game.GetWhoseTurn() == Who::BLACK) {
    goodness = -goodness;
  }
  return goodness;
}

std::vector<Move> history;

MinimaxResult Minimax(const Game& game, int depth, bool isMaximizingPlayer,
                      qreal alpha, qreal beta) {
  if (depth == 3 || game.IsGameOver()) {
    qreal val = GameGoodness(game);
    return {val, Move::Skip()};
  }
  const std::vector<Move> moves = GenPossibleMoves(game);
  qreal best_val = isMaximizingPlayer ? std::numeric_limits<qreal>::lowest()
                                      : std::numeric_limits<qreal>::max();
  Move best_move = Move::Skip();
  for (const Move& move : moves) {
    Game clone = game.Clone();
    clone.MakeMove(move);
    history.push_back(move);
    const auto res =
        Minimax(clone, depth + 1, !isMaximizingPlayer, alpha, beta);
    qreal val = res.value;
    if (isMaximizingPlayer) {
      if (val > best_val) {
        best_val = val;
        best_move = move;
      }
      alpha = std::max(alpha, val);
    } else {
      if (val < best_val) {
        best_val = val;
        best_move = move;
      }
      beta = std::min(beta, val);
    }
    history.pop_back();
    if (beta < alpha) {
      break;
    }
  }
  return {best_val, best_move};
}

AiPlayer::AiPlayer(Game* game, Controller* controller)
    : SyncPlayer(controller), game_(game) {}

Move AiPlayer::GetNextMoveImpl() {
  const auto res = Minimax(
      *game_, 0, /*is_maximizing_player=*/game_->GetWhoseTurn() == Who::WHITE,
      std::numeric_limits<qreal>::lowest(), std::numeric_limits<qreal>::max());
  return res.move;
}

}  // namespace hive
