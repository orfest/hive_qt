#include "mainwindow.h"
#include "aiplayer.h"
#include "humanplayer.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  game_ = std::make_unique<hive::Game>();
  game_->Init(hive::GameMode::NORMAL);
  selection_state_ = std::make_unique<hive::SelectionState>(this);
  controller_ = std::make_unique<hive::Controller>(game_.get());
  white_player_ = std::make_unique<hive::HumanPlayer>(selection_state_.get());
  black_player_ =
      std::make_unique<hive::AiPlayer>(game_.get(), controller_.get());
  controller_->SetPlayers(white_player_.get(), black_player_.get());

  ui->field->SetGame(game_.get())
      .SetCells(&game_->GetField().GetCells())
      .SetSelectionState(selection_state_.get())
      .SetController(controller_.get());
  ui->pieces_white->SetGame(game_.get())
      .SetSelectionState(selection_state_.get())
      .SetPiecesSet(&game_->GetPiecesSet(hive::Who::WHITE))
      .SetWho(hive::Who::WHITE);
  ui->pieces_black->SetGame(game_.get())
      .SetSelectionState(selection_state_.get())
      .SetPiecesSet(&game_->GetPiecesSet(hive::Who::BLACK))
      .SetWho(hive::Who::BLACK);

  controller_->Start();
}

MainWindow::~MainWindow() { delete ui; }
