#include "pieceswidget.h"

#include <cassert>

#include <QPaintEvent>
#include <QPainter>

#include "bugsicons.h"
#include "cellshape.h"

namespace hive {

constexpr const qreal PiecesWidget::kVerticalOverlap;

PiecesWidget::PiecesWidget(QWidget* parent) : QWidget(parent) {}

PiecesWidget& PiecesWidget::SetGame(Game* game) {
  Q_ASSERT(!game_);
  game_ = game;
  Q_ASSERT(game_);
  return *this;
}

PiecesWidget& PiecesWidget::SetPiecesSet(const PiecesSet* pieces_set) {
  Q_ASSERT(!pieces_set_);
  pieces_set_ = pieces_set;
  Q_ASSERT(pieces_set_);
  return *this;
}

PiecesWidget& PiecesWidget::SetSelectionState(SelectionState* selection_state) {
  Q_ASSERT(!selection_state_);
  selection_state_ = selection_state;
  Q_ASSERT(selection_state_);
  return *this;
}

PiecesWidget& PiecesWidget::SetWho(Who who) {
  Q_ASSERT(!who_);
  who_ = who;
  return *this;
}

Who PiecesWidget::GetWho() const {
  Q_ASSERT(who_);
  return *who_;
}

void PiecesWidget::paintEvent(QPaintEvent*) {
  Q_ASSERT(pieces_set_);
  int mx_cnt = 0;
  for (const auto& kv : pieces_set_->GetBugs()) {
    Q_ASSERT(kv.second > 0);
    mx_cnt = std::max(mx_cnt, kv.second);
  }

  QPainter painter;
  painter.begin(this);
  painter.setRenderHint(QPainter::Antialiasing);
  const int kBackgroundColor = 220;
  painter.fillRect(rect(), QBrush(QColor(kBackgroundColor, kBackgroundColor,
                                         kBackgroundColor)));
  if (!pieces_set_->GetBugs().empty()) {
    const int n = static_cast<int>(pieces_set_->GetBugs().size());
    scale_ = GetScale(n);
    int cnt = 0;
    const qreal y = height() - CellShape::HALF_DY * scale_;
    qreal x = CellShape::DX * 2 * scale_ / 3.0;
    const QPainterPath path = CellShape::GetInstance().GetCellPath();
    const QBrush brush = BugsIcons::GetInstance().GetCellBrush(GetWho());
    for (auto it = pieces_set_->GetBugs().begin();
         it != pieces_set_->GetBugs().end(); ++it) {
      QImage icon = BugsIcons::GetInstance().GetIcon(it->first, GetWho());
      for (int i = 0; i < it->second; i++) {
        painter.resetTransform();
        const int yy = static_cast<int>(y - i * CellShape::HALF_DY * scale_ *
                                                (1 - kVerticalOverlap));
        painter.translate(x, yy);
        painter.scale(scale_, scale_);
        painter.fillPath(path, brush);
        const int kPenColor = 127;
        painter.setPen(QPen(QColor(kPenColor, kPenColor, kPenColor)));
        painter.drawPath(path);
        if (it->first == selection_state_->GetAddBugSelection() &&
            i == it->second - 1 && game_->GetWhoseTurn() == GetWho()) {
          icon.invertPixels();
        }
        painter.drawImage(static_cast<int>(-icon.width() * 0.5),
                          static_cast<int>(-icon.height() * 0.5), icon);
      }
      x += CellShape::X_SIZE * scale_;
      cnt++;
    }
  }
}

qreal PiecesWidget::GetScale(int n) const {
  qreal scale = 1;
  if (CellShape::HALF_DY * 2 * (1 + 2 * (1 - kVerticalOverlap)) > height()) {
    scale = std::min(
        scale, height() / static_cast<qreal>(CellShape::HALF_DY * 2 *
                                             (1 + 2 * (1 - kVerticalOverlap))));
  }
  if (CellShape::X_SIZE * (1 + (n - 1)) > width()) {
    scale = std::min(
        scale, width() / static_cast<qreal>(CellShape::X_SIZE * (1 + (n - 1))));
  }
  return scale;
}

void PiecesWidget::mousePressEvent(QMouseEvent* event) {
  if (event->button() == Qt::RightButton) {
    selection_state_->ClearSelection();
    return;
  }
  if (event->button() != Qt::LeftButton ||
      (selection_state_->GetSelectionType() != SelectionType::NONE &&
       selection_state_->GetSelectionType() != SelectionType::ADD_TILE) ||
      game_->GetWhoseTurn() != GetWho() || game_->IsGameOver()) {
    return;
  }
  const auto& maybe_bug = GetClickedBug(event->pos());
  if (maybe_bug) {
    selection_state_->SetAddBugSelection(*maybe_bug);
  } else {
    selection_state_->ClearSelection();
  }
}

std::experimental::optional<Bug> PiecesWidget::GetClickedBug(
    const QPoint& pos) const {
  Q_ASSERT(pieces_set_);
  Q_ASSERT(game_);
  const int n = static_cast<int>(pieces_set_->GetBugs().size());
  const int k = static_cast<int>(pos.x() / (scale_ * CellShape::X_SIZE));
  if (k < 0 || k >= n) {
    return std::experimental::nullopt;
  }
  auto it = pieces_set_->GetBugs().begin();
  std::advance(it, k);
  Q_ASSERT(it != pieces_set_->GetBugs().end());
  const int v = it->second;
  if (pos.y() < height() - 2 * CellShape::HALF_DY * scale_ *
                               (1 + (v - 1) * (1 - kVerticalOverlap))) {
    return std::experimental::nullopt;
  }
  return it->first;
}

}  // namespace hive
