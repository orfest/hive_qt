#include "bugsicons.h"

#include <cassert>
#include <string>
#include <vector>

#include <QImageReader>

#include "cellshape.h"
#include "math.h"

// Q_INIT_RESOURCE doesn't work in a namespace.
void InitResource() { Q_INIT_RESOURCE(bugs); }

namespace hive {

/*static*/ const BugsIcons& BugsIcons::GetInstance() {
  static BugsIcons* instance = new BugsIcons();
  return *instance;
}

BugsIcons::BugsIcons() {
  // Q_INIT_RESOURCE doesn't work in a namespace.
  InitResource();
  // A mapping between Tile types and their icons.
  const std::vector<std::pair<Bug, QString>> bugs_and_filenames = {
      {Bug::ANT, ":/bugs/ant.png"},
      {Bug::BEETLE, ":/bugs/beetle.png"},
      {Bug::GRASSHOPPER, ":/bugs/grasshopper.png"},
      {Bug::QUEEN, ":/bugs/queen.png"},
      {Bug::SPIDER, ":/bugs/spider.png"},
      {Bug::LADYBUG, ":/bugs/ladybug.png"},
      {Bug::MOSQUITO, ":/bugs/mosquito.png"},
      {Bug::PILLBUG, ":/bugs/pillbug.png"},
  };
  for (const auto& bug_and_filename : bugs_and_filenames) {
    QImageReader reader(bug_and_filename.second);
    Q_ASSERT(reader.canRead());
    icons_[bug_and_filename.first] = reader.read();
  }
}

QImage BugsIcons::GetIcon(Bug bug, Who who) const {
  auto it = icons_.find(bug);
  Q_ASSERT(it != icons_.end());
  // Scale the icon to be completely inside the hex cell.
  QImage icon = it->second.scaled(int(CellShape::X_SIZE * 0.6),
                                  int(CellShape::HALF_DY * 2 * 0.6));
  if (who == Who::BLACK) {
    // Icons are for white tiles, invert colors for black tiles.
    icon.invertPixels();
  }
  return icon;
}

QBrush BugsIcons::GetCellBrush(Who who) const {
  const int color = who == Who::WHITE ? 255  // white
                                      : 0;   // black;
  return QBrush(QColor(color, color, color));
}

}  // namespace hive
