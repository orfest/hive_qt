#include "syncplayer.h"

namespace hive {

SyncPlayer::SyncPlayer(Controller* controller) : controller_(controller) {}

void SyncPlayer::GetAndMakeMove() { controller_->MakeMove(GetNextMove()); }

void SyncPlayer::EndMoveImpl() {}

}  // namespace hive
