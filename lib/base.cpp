#include "math.h"

#include <cmath>

#include <QtGlobal>

namespace hive {

qreal ExactRealMod(qreal a, qreal b) {
  Q_ASSERT(b > 0);
  qreal r = fmod(a, b);
  if (r < 0) {
    r += b;
  }
  return r;
}

qreal ExactRealDiv(qreal a, qreal b) {
  Q_ASSERT(b > 0);
  return round((a - ExactRealMod(a, b)) / b);
}

bool IsIntEven(int x) { return !(x & 1); }

}  // namespace hive
