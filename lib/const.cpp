#include "const.h"

#include <QtGlobal>

namespace hive {
namespace {

std::string BugToString(Bug bug) {
  switch (bug) {
    case Bug::QUEEN:
      return "Queen";
    case Bug::SPIDER:
      return "Spider";
    case Bug::BEETLE:
      return "Beetle";
    case Bug::GRASSHOPPER:
      return "Grasshopper";
    case Bug::ANT:
      return "Ant";
    case Bug::MOSQUITO:
      return "Mosquito";
    case Bug::LADYBUG:
      return "Ladybug";
    case Bug::PILLBUG:
      return "Pillbug";
  }
}

std::string WhoToString(Who who) {
  switch (who) {
    case Who::WHITE:
      return "WHITE";
    case Who::BLACK:
      return "BLACK";
  }
}

std::string GameModeToString(GameMode game_mode) {
  switch (game_mode) {
    case GameMode::EXPANSION:
      return "EXPANSION";
    case GameMode::NORMAL:
      return "NORMAL";
    case GameMode::PILLBUG:
      return "PILLBUG";
  }
}

}  // namespace

Who GetEnemy(Who who) {
  switch (who) {
    case Who::WHITE:
      return Who::BLACK;
    case Who::BLACK:
      return Who::WHITE;
  }
}

QDebug operator<<(QDebug dbg, Bug bug) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << QString::fromStdString(BugToString(bug));
  return dbg;
}

std::ostream& operator<<(std::ostream& os, Bug bug) {
  return os << BugToString(bug);
}

QDebug operator<<(QDebug dbg, Who who) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << QString::fromStdString(WhoToString(who));
  return dbg;
}

std::ostream& operator<<(std::ostream& os, Who who) {
  return os << WhoToString(who);
}

QDebug operator<<(QDebug dbg, GameMode game_mode) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << QString::fromStdString(GameModeToString(game_mode));
  return dbg;
}

std::ostream& operator<<(std::ostream& os, GameMode game_mode) {
  return os << GameModeToString(game_mode);
}

}  // namespace hive
