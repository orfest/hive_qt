#include "hexpos.h"

#include <QtDebug>
#include <QtGlobal>

namespace hive {
namespace {

// HexPosinate change for each of the kNumDirs directions.
const int dx[] = {1, 0, -1, -1, 0, 1};
const int dy[] = {0, 1, 1, 0, -1, -1};

}  // namespace

const int HexPos::kNumDirs;

HexPos::HexPos(int x, int y) : x_(x), y_(y) {}
HexPos::HexPos()
    : x_(std::numeric_limits<int>::max()),
      y_(std::numeric_limits<int>::max()) {}

int HexPos::GetX() const { return x_; }

int HexPos::GetY() const { return y_; }

HexPos HexPos::GetDirNeighbor(int dir) const {
  Q_ASSERT(dir >= 0 && dir < kNumDirs);
  return HexPos(x_ + dx[dir], y_ + dy[dir]);
}

std::vector<HexPos> HexPos::GetNeighbors() const {
  std::vector<HexPos> result;
  for (int i = 0; i < kNumDirs; i++) {
    result.push_back(HexPos(x_ + dx[i], y_ + dy[i]));
  }
  return result;
}

bool HexPos::operator<(const HexPos& hexpos) const {
  if (x_ != hexpos.x_) {
    return x_ < hexpos.x_;
  }
  return y_ < hexpos.y_;
}

bool HexPos::operator==(const HexPos& hexpos) const {
  return x_ == hexpos.x_ && y_ == hexpos.y_;
}

bool HexPos::operator!=(const HexPos& hexpos) const {
  return x_ != hexpos.x_ || y_ != hexpos.y_;
}

int HexPos::NextDir(int dir) {
  Q_ASSERT(dir >= 0 && dir < kNumDirs);
  return (dir + 1) % kNumDirs;
}

int HexPos::PrevDir(int dir) {
  Q_ASSERT(dir >= 0 && dir < kNumDirs);
  return (dir + kNumDirs - 1) % kNumDirs;
}

QDebug operator<<(QDebug dbg, const HexPos& hexpos) {
  QDebugStateSaver saver(dbg);
  dbg.nospace() << "HexPos{" << hexpos.GetX() << ", " << hexpos.GetY() << "}";
  return dbg;
}

std::ostream& operator<<(std::ostream& os, const HexPos& hexpos) {
  return os << "HexPos{" << hexpos.GetX() << ", " << hexpos.GetY() << "}";
}

}  // namespace hive
