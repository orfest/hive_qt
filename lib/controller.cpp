#include "controller.h"

namespace hive {

Controller::Controller(Game* game) : game_(game) {}

void Controller::SetPlayers(Player* white_player, Player* black_player) {
  players_ = {{Who::WHITE, white_player}, {Who::BLACK, black_player}};
}

void Controller::Start() { NotifyNextMove(game_->GetWhoseTurn()); }

void Controller::NotifyNextMove(Who who) {
  Q_ASSERT(players_.count(who));
  for (const auto& kv : players_) {
    if (kv.first == who) {
      kv.second->MakeMove();
    } else {
      kv.second->EndMove();
    }
  }
}

void Controller::NotifyGameOver() {
  for (const auto& kv : players_) {
    kv.second->EndMove();
  }
}

void Controller::MakeMove(const Move& move) {
  game_->MakeMove(move);
  if (game_->IsGameOver()) {
    NotifyGameOver();
  } else {
    NotifyNextMove(game_->GetWhoseTurn());
  }
}

}  // namespace hive
