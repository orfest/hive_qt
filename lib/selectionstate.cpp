#include "selectionstate.h"

namespace hive {

SelectionState::SelectionState(QWidget* main_window)
    : main_window_(main_window) {}

void SelectionState::SetAddBugSelection(Bug bug) {
  ClearSelectionInternal();
  add_bug_selection_ = bug;
  Repaint();
}

void SelectionState::SetMoveBugSelection(const HexPos& hexpos) {
  ClearSelectionInternal();
  move_bug_selection_ = hexpos;
  Repaint();
}

void SelectionState::SetPillbugSelection(const HexPos& hexpos) {
  ClearSelectionInternal();
  pillbug_selection_ = hexpos;
  Repaint();
}

void SelectionState::SetPillbugAndTargetSelection(const HexPos& pillbug_pos,
                                                  const HexPos& target_pos) {
  ClearSelectionInternal();
  pillbug_selection_ = pillbug_pos;
  move_bug_selection_ = target_pos;
  Repaint();
}

void SelectionState::ClearSelectionInternal() {
  add_bug_selection_ = std::experimental::nullopt;
  move_bug_selection_ = std::experimental::nullopt;
  pillbug_selection_ = std::experimental::nullopt;
}

void SelectionState::ClearSelection() {
  ClearSelectionInternal();
  Repaint();
}

SelectionType SelectionState::GetSelectionType() const {
  if (pillbug_selection_) {
    return SelectionType::PILLBUG_ABILITY;
  } else if (add_bug_selection_) {
    return SelectionType::ADD_TILE;
  } else if (move_bug_selection_) {
    return SelectionType::MOVE;
  } else {
    return SelectionType::NONE;
  }
}

void SelectionState::Repaint() {
  if (main_window_) {
    main_window_->repaint();
  }
}

}  // namespace hive
