#include "cellshape.h"

namespace hive {

const qreal CellShape::DX;
const qreal CellShape::HALF_DY;
const qreal CellShape::X_SIZE;

namespace {
const std::vector<QPointF> kVertices = {
    QPointF(-CellShape::DX / 3.0, CellShape::HALF_DY),
    QPointF(CellShape::DX / 3.0, CellShape::HALF_DY),
    QPointF(CellShape::DX * 2 / 3.0, 0),
    QPointF(CellShape::DX / 3.0, -CellShape::HALF_DY),
    QPointF(-CellShape::DX / 3.0, -CellShape::HALF_DY),
    QPointF(-CellShape::DX * 2 / 3.0, 0),
};
}  // namespace

/*static*/ const CellShape& CellShape::GetInstance() {
  static const CellShape* instance = new CellShape();
  return *instance;
}

CellShape::CellShape() {
  cell_path_.moveTo(kVertices.back());
  for (const auto& p : kVertices) {
    cell_path_.lineTo(p);
  }
}

const QPainterPath& CellShape::GetCellPath() const { return cell_path_; }

}  // namespace hive
