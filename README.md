# Overview

hive_qt is a client for playing [Hive](http://www.gen42.com/downloads/rules/Hive_Rules.pdf).
Hive is a two-player game similar to chess, that is not restricted by a board
and can be played anywhere on any flat surface. Please read the rules before
playing the game.

## Extensions

hive_qt supports the basic Hive version, and also the Mosquito, Ladybug, Pillbug
extensions.

# Installation

Please download and compile hive_qt source code.

## Dependencies

To compile and run the Qt Client, you need qt5.

To compile and run the tests you need qt5, gtest and gmock.

## Compiler

C++ compiler needs to support c++1y.

Tested on gcc-5.4.0, gcc-7.3.0 and clang-5.0.1.
