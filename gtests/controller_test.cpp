#include <gtest/gtest.h>

#include <QtDebug>
#include <memory>

#include "controller.h"
#include "mock_game.h"
#include "mock_player.h"

namespace hive {
namespace {

using ::testing::Return;
using ::testing::_;

class ControllerTest : public ::testing::Test {
 public:
  void SetUp() override {
    controller_ = std::make_unique<Controller>(&mock_game_);
    controller_->SetPlayers(&mock_player_white_, &mock_player_black_);
  }

  void TearDown() override { controller_.reset(); }

 protected:
  testing::MockPlayer mock_player_white_;
  testing::MockPlayer mock_player_black_;
  testing::MockGame mock_game_;
  std::unique_ptr<Controller> controller_;
};

TEST_F(ControllerTest, Start) {
  EXPECT_CALL(mock_player_white_, GetAndMakeMove()).Times(1);
  EXPECT_CALL(mock_player_black_, EndMoveImpl()).Times(1);
  EXPECT_CALL(mock_game_, IsGameOver()).Times(0);
  EXPECT_CALL(mock_game_, GetWhoseTurn()).WillOnce(Return(Who::WHITE));
  controller_->Start();
}

TEST_F(ControllerTest, GameOver) {
  EXPECT_CALL(mock_player_white_, EndMoveImpl()).Times(1);
  EXPECT_CALL(mock_player_black_, EndMoveImpl()).Times(1);
  EXPECT_CALL(mock_game_, IsGameOver()).WillOnce(Return(true));
  EXPECT_CALL(mock_game_, MakeMove(_)).Times(1);
  controller_->MakeMove(Move::Skip());
}

TEST_F(ControllerTest, Move) {
  EXPECT_CALL(mock_player_white_, GetAndMakeMove()).Times(0);
  EXPECT_CALL(mock_player_white_, EndMoveImpl()).Times(1);
  EXPECT_CALL(mock_player_black_, GetAndMakeMove()).Times(1);
  EXPECT_CALL(mock_player_black_, EndMoveImpl()).Times(0);
  EXPECT_CALL(mock_game_, IsGameOver()).WillOnce(Return(false));
  EXPECT_CALL(mock_game_, MakeMove(_)).Times(1);
  EXPECT_CALL(mock_game_, GetWhoseTurn()).WillOnce(Return(Who::BLACK));
  controller_->MakeMove(Move::Skip());
}

}  // namespace
}  // namespace hive
