#include <gtest/gtest.h>

#include "tile.h"

namespace hive {
namespace {

TEST(TileTest, Getters) {
  EXPECT_EQ(Tile(Who::WHITE, Bug::ANT).GetBug(), Bug::ANT);
  EXPECT_EQ(Tile(Who::WHITE, Bug::ANT).GetWho(), Who::WHITE);
}

}  // namespace
}  // namespace hive
