#include <vector>

#include <gtest/gtest.h>

#include <QImage>

#include "bugsicons.h"
#include "const.h"

namespace hive {
namespace {

TEST(BugsIconsTest, IconsAreDifferent) {
  std::vector<QImage> icons;
  for (Bug bug : {
           Bug::QUEEN,
           Bug::SPIDER,
           Bug::BEETLE,
           Bug::GRASSHOPPER,
           Bug::ANT,
           Bug::MOSQUITO,
           Bug::LADYBUG,
           Bug::PILLBUG,
       }) {
    for (Who who : {Who::WHITE, Who::BLACK}) {
      icons.push_back(BugsIcons::GetInstance().GetIcon(bug, who));
    }
  }
  // Verify that different bugs have different icons, and that icons of bugs of
  // different players are always different.
  for (auto it = icons.begin(); it != icons.end(); ++it) {
    auto another_it = it;
    EXPECT_TRUE(*it == *another_it);
    for (++another_it; another_it != icons.end(); ++another_it) {
      EXPECT_TRUE(*it != *another_it);
    }
  }
}

}  // namespace
}  // namespace hive
