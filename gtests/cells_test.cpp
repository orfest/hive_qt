#include <gtest/gtest.h>

#include "cells.h"

namespace hive {
namespace {

TEST(CellsTest, SizeAndIsEmptyAndClear) {
  Cells cells;
  EXPECT_TRUE(cells.IsEmpty());
  EXPECT_EQ(cells.Size(), 0);

  cells.AddTile(HexPos(1, 2), Tile(Who::WHITE, Bug::MOSQUITO));
  EXPECT_FALSE(cells.IsEmpty());
  EXPECT_EQ(cells.Size(), 1);

  cells.AddTile(HexPos(1, 2), Tile(Who::WHITE, Bug::QUEEN));
  EXPECT_FALSE(cells.IsEmpty());
  EXPECT_EQ(cells.Size(), 1);

  cells.AddTile(HexPos(0, 0), Tile(Who::BLACK, Bug::ANT));
  EXPECT_FALSE(cells.IsEmpty());
  EXPECT_EQ(cells.Size(), 2);

  cells.Clear();
  EXPECT_TRUE(cells.IsEmpty());
  EXPECT_EQ(cells.Size(), 0);
}

TEST(CellsTest, Stacks) {
  Cells cells;
  EXPECT_FALSE(cells.Has(HexPos(0, 0)));
  EXPECT_EQ(cells.StackHeight(HexPos(0, 0)), 0);
  EXPECT_TRUE(cells.GetTopTile(HexPos(0, 0)) == std::experimental::nullopt);

  const Tile tile(Who::BLACK, Bug::ANT);
  cells.AddTile(HexPos(0, 0), tile);
  EXPECT_TRUE(cells.Has(HexPos(0, 0)));
  EXPECT_EQ(cells.StackHeight(HexPos(0, 0)), 1);
  EXPECT_TRUE(cells.GetTopTile(HexPos(0, 0)) == tile);

  const Tile queen_tile(Who::BLACK, Bug::QUEEN);
  cells.AddTile(HexPos(0, 0), queen_tile);
  EXPECT_TRUE(cells.Has(HexPos(0, 0)));
  EXPECT_EQ(cells.StackHeight(HexPos(0, 0)), 2);
  EXPECT_TRUE(cells.GetTopTile(HexPos(0, 0)) == queen_tile);

  cells.RemoveTopTile(HexPos(0, 0));
  EXPECT_TRUE(cells.GetTopTile(HexPos(0, 0)) == tile);
}

TEST(CellsTest, IsConnected) {
  Cells cells;
  EXPECT_TRUE(cells.AreConnected());

  cells.AddTile(HexPos(0, 0), Tile(Who::BLACK, Bug::ANT));
  EXPECT_TRUE(cells.AreConnected());

  cells.AddTile(HexPos(0, 0), Tile(Who::WHITE, Bug::BEETLE));
  EXPECT_TRUE(cells.AreConnected());

  cells.AddTile(HexPos(2, 0), Tile(Who::WHITE, Bug::GRASSHOPPER));
  EXPECT_FALSE(cells.AreConnected());

  cells.AddTile(HexPos(1, 0), Tile(Who::BLACK, Bug::GRASSHOPPER));
  EXPECT_TRUE(cells.AreConnected());
}

TEST(CellsTest, InvalidValues) {
  Cells cells;
  EXPECT_DEATH(cells.RemoveTopTile(HexPos(0, 0)), "");  // Not present.

  EXPECT_DEATH(cells.GetTopTileOrDie(HexPos(0, 0)), "");
}

}  // namespace
}  // namespace hive
