#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "game.h"

namespace hive {
namespace {

using ::testing::SizeIs;

TEST(GameTest, Init) {
  Game game;
  game.Init(GameMode::NORMAL);
  EXPECT_THAT(game.GetPiecesSet(Who::WHITE).GetBugs(), SizeIs(5));
  EXPECT_THAT(game.GetPiecesSet(Who::BLACK).GetBugs(), SizeIs(5));

  game.Init(GameMode::EXPANSION);
  EXPECT_THAT(game.GetPiecesSet(Who::WHITE).GetBugs(), SizeIs(7));
  EXPECT_THAT(game.GetPiecesSet(Who::BLACK).GetBugs(), SizeIs(7));

  game.Init(GameMode::PILLBUG);
  EXPECT_THAT(game.GetPiecesSet(Who::WHITE).GetBugs(), SizeIs(8));
  EXPECT_THAT(game.GetPiecesSet(Who::BLACK).GetBugs(), SizeIs(8));
}

TEST(GameTest, IsGameOver) {
  Game game;
  game.Init(GameMode::NORMAL);
  EXPECT_FALSE(game.IsGameOver());
}

TEST(GameTest, AddQueen) {
  Game game;
  game.Init(GameMode::NORMAL);

  // White queen bee is not placed yet.
  EXPECT_FALSE(game.IsQueenPlaced());
  EXPECT_EQ(game.GetWhoseTurn(), Who::WHITE);
  game.MakeMove(Move::AddTile(Bug::QUEEN, HexPos(0, 0)));

  // Black queen bee is not placed yet.
  EXPECT_FALSE(game.IsQueenPlaced());
  EXPECT_EQ(game.GetWhoseTurn(), Who::BLACK);
  game.MakeMove(Move::AddTile(Bug::QUEEN, HexPos(1, 0)));

  // White queen bee is already placed.
  EXPECT_TRUE(game.IsQueenPlaced());
  EXPECT_FALSE(game.IsGameOver());
  EXPECT_EQ(game.GetWhoseTurn(), Who::WHITE);
}

}  // namespace
}  // namespace hive
