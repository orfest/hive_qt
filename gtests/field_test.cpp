#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "field.h"

namespace hive {
namespace {

using ::testing::UnorderedElementsAre;

TEST(FieldTest, IsQueenPlaced) {
  Field field;
  EXPECT_FALSE(field.IsQueenPlaced(Who::WHITE));
  EXPECT_FALSE(field.IsQueenPlaced(Who::BLACK));

  field.AddTile(Bug::QUEEN, Who::BLACK, HexPos(0, 0));
  EXPECT_FALSE(field.IsQueenPlaced(Who::WHITE));
  EXPECT_TRUE(field.IsQueenPlaced(Who::BLACK));

  field.AddTile(Bug::BEETLE, Who::WHITE, HexPos(0, 0));
  EXPECT_FALSE(field.IsQueenPlaced(Who::WHITE));
  EXPECT_TRUE(field.IsQueenPlaced(Who::BLACK));
}

TEST(FieldTest, AreQueensAlive) {
  Field field;
  EXPECT_TRUE(field.AreQueensAlive());

  field.AddTile(Bug::QUEEN, Who::BLACK, HexPos(0, 0));
  for (int i = 0; i < HexPos::kNumDirs; i++) {
    EXPECT_TRUE(field.AreQueensAlive());
    field.AddTile(Bug::ANT, Who::BLACK, HexPos(0, 0).GetDirNeighbor(i));
  }
  EXPECT_FALSE(field.AreQueensAlive());
}

TEST(FieldTest, HasPillbugAbility) {
  Field field;
  field.AddTile(Bug::QUEEN, Who::BLACK, HexPos(0, 0));
  EXPECT_FALSE(field.HasPillbugAbility(HexPos(0, 0)));

  field.AddTile(Bug::PILLBUG, Who::BLACK, HexPos(1, 0));
  EXPECT_FALSE(field.HasPillbugAbility(HexPos(0, 0)));
  EXPECT_TRUE(field.HasPillbugAbility(HexPos(1, 0)));

  field.AddTile(Bug::MOSQUITO, Who::WHITE, HexPos(0, 1));
  EXPECT_FALSE(field.HasPillbugAbility(HexPos(0, 0)));
  EXPECT_TRUE(field.HasPillbugAbility(HexPos(1, 0)));
  EXPECT_TRUE(field.HasPillbugAbility(HexPos(0, 1)));

  field.AddTile(Bug::BEETLE, Who::WHITE, HexPos(0, 1));
  EXPECT_FALSE(field.HasPillbugAbility(HexPos(0, 0)));
  EXPECT_TRUE(field.HasPillbugAbility(HexPos(1, 0)));
  EXPECT_FALSE(field.HasPillbugAbility(HexPos(0, 1)));
}

TEST(FieldTest, GetPossibleMovesNoQueenYet) {
  Field field;

  field.AddTile(Bug::ANT, Who::WHITE, HexPos(0, 0));
  EXPECT_TRUE(field.GetPossibleMoves(HexPos(0, 0)).empty());

  field.AddTile(Bug::ANT, Who::WHITE, HexPos(1, 0));
  EXPECT_TRUE(field.GetPossibleMoves(HexPos(0, 0)).empty());
}

TEST(FieldTest, GetPossibleMovesQueenFromRuleBook) {
  Field field;

  field.AddTile(Bug::GRASSHOPPER, Who::BLACK, HexPos(1, 0));
  field.AddTile(Bug::QUEEN, Who::BLACK, HexPos(2, 0));
  field.AddTile(Bug::ANT, Who::BLACK, HexPos(3, 0));
  field.AddTile(Bug::SPIDER, Who::WHITE, HexPos(4, 0));
  field.AddTile(Bug::SPIDER, Who::BLACK, HexPos(3, 1));
  field.AddTile(Bug::BEETLE, Who::WHITE, HexPos(2, 2));
  field.AddTile(Bug::BEETLE, Who::BLACK, HexPos(1, 2));
  field.AddTile(Bug::GRASSHOPPER, Who::WHITE, HexPos(0, 1));
  field.AddTile(Bug::ANT, Who::WHITE, HexPos(0, 2));
  field.AddTile(Bug::QUEEN, Who::WHITE, HexPos(0, 3));

  EXPECT_THAT(field.GetPossibleMoves(HexPos(2, 0)),
              UnorderedElementsAre(HexPos(1, 1), HexPos(2, 1), HexPos(2, -1),
                                   HexPos(3, -1)));
}

TEST(FieldTest, GetPossibleMovesBeetleFromRuleBook) {
  Field field;

  field.AddTile(Bug::QUEEN, Who::WHITE, HexPos(0, 0));
  field.AddTile(Bug::GRASSHOPPER, Who::WHITE, HexPos(0, 1));
  field.AddTile(Bug::ANT, Who::BLACK, HexPos(1, -1));
  field.AddTile(Bug::BEETLE, Who::WHITE, HexPos(2, -1));
  field.AddTile(Bug::SPIDER, Who::BLACK, HexPos(2, -2));

  EXPECT_THAT(field.GetPossibleMoves(HexPos(2, -1)),
              UnorderedElementsAre(HexPos(3, -2), HexPos(2, -2), HexPos(1, -1),
                                   HexPos(1, 0)));
}

}  // namespace
}  // namespace hive
