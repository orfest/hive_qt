#include <memory>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <QObject>

#include "selectionstate.h"

namespace hive {
namespace {

TEST(SelectionStateTest, AddTile) {
  SelectionState selection_state(nullptr);
  selection_state.SetAddBugSelection(Bug::ANT);
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::ADD_TILE);
  EXPECT_EQ(selection_state.GetAddBugSelection(), Bug::ANT);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), std::experimental::nullopt);
  EXPECT_EQ(selection_state.GetPillbugSelection(), std::experimental::nullopt);

  selection_state.SetAddBugSelection(Bug::BEETLE);
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::ADD_TILE);
  EXPECT_EQ(selection_state.GetAddBugSelection(), Bug::BEETLE);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), std::experimental::nullopt);
  EXPECT_EQ(selection_state.GetPillbugSelection(), std::experimental::nullopt);

  selection_state.ClearSelection();
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::NONE);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), std::experimental::nullopt);
  EXPECT_EQ(selection_state.GetPillbugSelection(), std::experimental::nullopt);
}

TEST(SelectionStateTest, SelectBug) {
  SelectionState selection_state(nullptr);
  selection_state.SetMoveBugSelection(HexPos(1, 2));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::MOVE);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), HexPos(1, 2));
  EXPECT_EQ(selection_state.GetPillbugSelection(), std::experimental::nullopt);

  selection_state.SetMoveBugSelection(HexPos(3, 2));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::MOVE);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), HexPos(3, 2));
  EXPECT_EQ(selection_state.GetPillbugSelection(), std::experimental::nullopt);
}

TEST(SelectionStateTest, SelectPillbug) {
  SelectionState selection_state(nullptr);
  selection_state.SetPillbugSelection(HexPos(1, 2));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::PILLBUG_ABILITY);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), std::experimental::nullopt);
  EXPECT_EQ(selection_state.GetPillbugSelection(), HexPos(1, 2));

  selection_state.SetPillbugSelection(HexPos(3, 2));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::PILLBUG_ABILITY);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), std::experimental::nullopt);
  EXPECT_EQ(selection_state.GetPillbugSelection(), HexPos(3, 2));
}

TEST(SelectionStateTest, SelectPillbugAndTarget) {
  SelectionState selection_state(nullptr);
  selection_state.SetPillbugAndTargetSelection(HexPos(1, 2), HexPos(2, 3));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::PILLBUG_ABILITY);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), HexPos(2, 3));
  EXPECT_EQ(selection_state.GetPillbugSelection(), HexPos(1, 2));

  selection_state.SetPillbugAndTargetSelection(HexPos(3, 2), HexPos(4, 3));
  EXPECT_EQ(selection_state.GetSelectionType(), SelectionType::PILLBUG_ABILITY);
  EXPECT_EQ(selection_state.GetMoveBugSelection(), HexPos(4, 3));
  EXPECT_EQ(selection_state.GetPillbugSelection(), HexPos(3, 2));
}

}  // namespace
}  // namespace hive
