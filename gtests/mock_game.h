#ifndef MOCK_GAME_H
#define MOCK_GAME_H

#include <gmock/gmock.h>

#include "const.h"
#include "game.h"
#include "move.h"

namespace hive {
namespace testing {

class MockGame : public Game {
 public:
  MOCK_CONST_METHOD0(IsGameOver, bool());
  MOCK_CONST_METHOD0(GetWhoseTurn, Who());
  MOCK_METHOD1(MakeMove, void(const Move&));
};

}  // namespace testing
}  // namespace hive

#endif  // MOCK_GAME_H
