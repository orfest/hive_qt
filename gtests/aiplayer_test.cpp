#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "aiplayer.h"
#include "mock_controller.h"

namespace hive {
namespace {

using ::testing::Property;

class AiPlayerTest : public ::testing::Test {
 public:
  void SetUp() override {
    game_ = std::make_unique<Game>();
    game_->Init(GameMode::NORMAL);
    ai_player_ = std::make_unique<AiPlayer>(game_.get(), &mock_controller_);
  }

  void TearDown() override {
    ai_player_.reset();
    game_.reset();
  }

 protected:
  std::unique_ptr<AiPlayer> ai_player_;
  std::unique_ptr<Game> game_;
  testing::MockController mock_controller_;
};

// Verify that AiPlayer selects the next turn and passes it to the controller.
TEST_F(AiPlayerTest, FirstTurn) {
  // As the field is empty, the first move will be of type ADD_TILE.
  EXPECT_CALL(mock_controller_,
              MakeMove(Property(&Move::GetMoveType, Move::MoveType::ADD_TILE)));
  ai_player_->MakeMove();
}

TEST_F(AiPlayerTest, FreeQueenToAvoidLosing) {
  game_->MakeMove(Move::AddTile(Bug::QUEEN, HexPos(0, 0)));       // White
  game_->MakeMove(Move::AddTile(Bug::QUEEN, HexPos(1, -1)));      // Black
  game_->MakeMove(Move::AddTile(Bug::ANT, HexPos(-1, 0)));        // White
  game_->MakeMove(Move::AddTile(Bug::SPIDER, HexPos(2, -2)));     // Black
  game_->MakeMove(Move::MoveTile(HexPos(-1, 0), HexPos(1, -2)));  // White
  game_->MakeMove(Move::AddTile(Bug::SPIDER, HexPos(3, -3)));     // Black
  game_->MakeMove(Move::AddTile(Bug::ANT, HexPos(-1, 0)));        // White
  game_->MakeMove(Move::AddTile(Bug::BEETLE, HexPos(4, -3)));     // Black
  game_->MakeMove(Move::AddTile(Bug::ANT, HexPos(0, 1)));         // White
  game_->MakeMove(Move::AddTile(Bug::BEETLE, HexPos(2, -1)));     // Black
  game_->MakeMove(Move::MoveTile(HexPos(-1, 0), HexPos(0, -1)));  // White
  EXPECT_NE(ai_player_->GetNextMove().GetMoveType(), Move::MoveType::ADD_TILE);
}

}  // namespace
}  // namespace hive
