QT += testlib widgets
QT -= gui

TARGET = gtests

CONFIG += c++14 console qt warn_on depend_includepath testcase
CONFIG -= app_bundle
QMAKE_CXXFLAGS += -Wall -Wextra

TEMPLATE = app

SOURCES += \
    hexpos_test.cpp \
    main.cpp \
    bugsicons_test.cpp \
    cells_test.cpp \
    cellshape_test.cpp \
    selectionstate_test.cpp \
    field_test.cpp \
    tile_test.cpp \
    controller_test.cpp \
    game_test.cpp \
    aiplayer_test.cpp \
    math_test.cpp \
    coordinateconverter_test.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
LIBS += -lgtest -lgmock

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../lib/release/ -llib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../lib/debug/ -llib
else:unix: LIBS += -L$$OUT_PWD/../lib/ -llib

INCLUDEPATH += $$PWD/../lib/include
DEPENDPATH += $$PWD/../lib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lib/release/liblib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lib/debug/liblib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lib/release/lib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lib/debug/lib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../lib/liblib.a

HEADERS += \
    mock_player.h \
    mock_game.h \
    mock_controller.h
