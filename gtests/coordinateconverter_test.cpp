#include <gtest/gtest.h>

#include "cellshape.h"
#include "coordinateconverter.h"

namespace hive {
namespace {

using ModelPoint = CoordinateConverter::ModelPoint;

TEST(CoordinateConverterTest, ModelPointToHexPos) {
  CellShape::GetInstance();
  ASSERT_LT(0.1, CellShape::DX);
  ASSERT_LT(0.1, CellShape::HALF_DY);

  // (0,0) and small values around it are mapped to HexPos(0,0).
  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(ModelPoint(0, 0)),
            HexPos(0, 0));
  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(ModelPoint(-0.1, -0.1)),
            HexPos(0, 0));
  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(ModelPoint(0.1, -0.1)),
            HexPos(0, 0));
  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(ModelPoint(0.1, 0.1)),
            HexPos(0, 0));
  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(ModelPoint(-0.1, 0.1)),
            HexPos(0, 0));

  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(
                ModelPoint(CellShape::DX, CellShape::HALF_DY)),
            HexPos(0, 1));

  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(
                ModelPoint(0, 2 * CellShape::HALF_DY)),
            HexPos(1, 0));

  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(
                ModelPoint(-CellShape::DX, -CellShape::HALF_DY)),
            HexPos(0, -1));

  EXPECT_EQ(CoordinateConverter::ModelPointToHexPos(
                ModelPoint(0, -2 * CellShape::HALF_DY)),
            HexPos(-1, 0));
}

TEST(CoordinateConverterTest, HexPosToModelPoint) {
  EXPECT_EQ(CoordinateConverter::HexPosToModelPoint(HexPos(0, 0)),
            ModelPoint(0, 0));
  EXPECT_EQ(CoordinateConverter::HexPosToModelPoint(HexPos(0, 1)),
            ModelPoint(CellShape::DX, CellShape::HALF_DY));
  EXPECT_EQ(CoordinateConverter::HexPosToModelPoint(HexPos(1, 0)),
            ModelPoint(0, 2 * CellShape::HALF_DY));
}

}  // namespace
}  // namespace hive
