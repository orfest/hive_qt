#ifndef MOCK_CONTROLLER_H
#define MOCK_CONTROLLER_H

#include <gmock/gmock.h>

#include "controller.h"
#include "move.h"

namespace hive {
namespace testing {

class MockController : public Controller {
 public:
  MockController() : Controller(nullptr) {}
  ~MockController() override {}

  MOCK_METHOD0(Start, void());
  MOCK_METHOD1(MakeMove, void(const Move&));
};

}  // namespace testing
}  // namespace hive

#endif  // MOCK_CONTROLLER_H
