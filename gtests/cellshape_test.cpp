#include <gtest/gtest.h>

#include "cellshape.h"

namespace hive {
namespace {

// Verify that CellShape returns some non-trivial QPainterPath.
TEST(CellShapeTest, NonEmpty) {
  const auto& path = CellShape::GetInstance().GetCellPath();
  EXPECT_FALSE(path.isEmpty());
  EXPECT_GT(path.boundingRect().width(), 0);
  EXPECT_GT(path.boundingRect().height(), 0);

  EXPECT_DOUBLE_EQ(path.boundingRect().width(), CellShape::X_SIZE);
  EXPECT_DOUBLE_EQ(path.boundingRect().height(), CellShape::HALF_DY * 2);
}

}  // namespace
}  // namespace hive
