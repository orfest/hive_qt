#ifndef MOCK_PLAYER_H
#define MOCK_PLAYER_H

#include <gmock/gmock.h>

#include "player.h"

namespace hive {
namespace testing {

class MockPlayer : public Player {
 public:
  MOCK_METHOD0(GetAndMakeMove, void());
  MOCK_METHOD0(EndMoveImpl, void());
};

}  // namespace testing
}  // namespace hive

#endif  // MOCK_PLAYER_H
