#include <gtest/gtest.h>

#include "math.h"

namespace hive {
namespace {

TEST(MathTest, FgoodMod) {
  EXPECT_EQ(ExactRealMod(3, 2), 1);
  EXPECT_EQ(ExactRealMod(1, 2), 1);
  EXPECT_EQ(ExactRealMod(-1, 2), 1);
  EXPECT_EQ(ExactRealMod(-3, 2), 1);

  EXPECT_EQ(ExactRealMod(3.5, 2.5), 1);
  EXPECT_EQ(ExactRealMod(1.5, 2.5), 1.5);
  EXPECT_EQ(ExactRealMod(-1.5, 2.5), 1);
  EXPECT_EQ(ExactRealMod(-3.5, 2.5), 1.5);
}

TEST(MathTest, FgoodDiv) {
  EXPECT_EQ(ExactRealDiv(3, 2), 1);
  EXPECT_EQ(ExactRealDiv(1, 2), 0);
  EXPECT_EQ(ExactRealDiv(-1, 2), -1);
  EXPECT_EQ(ExactRealDiv(-3, 2), -2);

  EXPECT_EQ(ExactRealDiv(3.5, 2.5), 1);
  EXPECT_EQ(ExactRealDiv(1.5, 2.5), 0);
  EXPECT_EQ(ExactRealDiv(-1.5, 2.5), -1);
  EXPECT_EQ(ExactRealDiv(-3.5, 2.5), -2);
}

TEST(MathTest, IsEven) {
  EXPECT_TRUE(IsIntEven(0));
  EXPECT_FALSE(IsIntEven(-1));
  EXPECT_FALSE(IsIntEven(1));
  EXPECT_TRUE(IsIntEven(-2));
  EXPECT_TRUE(IsIntEven(2));
}

}  // namespace
}  // namespace hive
