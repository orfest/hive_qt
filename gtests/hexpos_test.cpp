#include <gtest/gtest.h>

#include "hexpos.h"

namespace hive {
namespace {

TEST(HexPosTest, Neighbors) {
  HexPos valid(10, 20);
  auto neighbors = valid.GetNeighbors();
  EXPECT_EQ(static_cast<int>(neighbors.size()), HexPos::kNumDirs);
  for (int i = 0; i < HexPos::kNumDirs; i++) {
    EXPECT_EQ(neighbors[static_cast<size_t>(i)], valid.GetDirNeighbor(i));
  }
}

TEST(HexPosTest, Directions) {
  for (int i = 0; i < HexPos::kNumDirs; i++) {
    const int next_dir = HexPos::NextDir(i);
    EXPECT_TRUE(next_dir >= 0 && next_dir < HexPos::kNumDirs && next_dir != i);
    const int prev_dir = HexPos::PrevDir(i);
    EXPECT_TRUE(prev_dir >= 0 && prev_dir < HexPos::kNumDirs && prev_dir != i &&
                next_dir != prev_dir);
  }
  EXPECT_DEATH(HexPos::NextDir(HexPos::kNumDirs), "");
  EXPECT_DEATH(HexPos::PrevDir(HexPos::kNumDirs), "");
  EXPECT_DEATH(HexPos::NextDir(-1), "");
  EXPECT_DEATH(HexPos::PrevDir(-1), "");
}

TEST(HexPosTest, Comparison) {
  EXPECT_TRUE(HexPos(0, 0) == HexPos(0, 0));
  EXPECT_FALSE(HexPos(0, 0) == HexPos(1, 0));
  EXPECT_FALSE(HexPos(0, 0) == HexPos(-1, 0));
  EXPECT_FALSE(HexPos(0, 0) == HexPos(0, 1));
  EXPECT_FALSE(HexPos(0, 0) == HexPos(0, -1));

  EXPECT_FALSE(HexPos(0, 0) != HexPos(0, 0));
  EXPECT_TRUE(HexPos(0, 0) != HexPos(1, 0));
  EXPECT_TRUE(HexPos(0, 0) != HexPos(-1, 0));
  EXPECT_TRUE(HexPos(0, 0) != HexPos(0, 1));
  EXPECT_TRUE(HexPos(0, 0) != HexPos(0, -1));

  EXPECT_TRUE(HexPos(-10, -10) < HexPos(10, 10));
  EXPECT_FALSE(HexPos(10, 10) < HexPos(-10, -10));
  EXPECT_FALSE(HexPos(0, 0) < HexPos(0, 0));
}

}  // namespace
}  // namespace hive
